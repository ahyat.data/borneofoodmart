import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/theme/app_theme.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'app/data/providers/auth_provider.dart';
import 'app/modules/authentication/auth_service.dart';
import 'app/routes/app_pages.dart';

Future<void> main() async {
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      theme: appTheme,
      color: mainColor,
      initialBinding: MainBindings(),
    );
  }
}

class MainBindings extends Bindings{
  @override
  void dependencies() {
    Get.put(AuthProvider(), permanent: true);
    Get.put(AuthService(), permanent: true);
    Get.put(AuthController(), permanent: true);
  }

}