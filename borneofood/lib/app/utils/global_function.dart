import 'package:borneofood/app/theme/color_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

void showLoadingWidget() {
  Get.dialog(
      Center(
          child: SpinKitFadingFour(
            color: mainColor,
            size: 50,
          )
      )
  );
}

void closeLoadingWidget(){
  Get.back();
}