/// id : 1
/// name : "Alamat Utama"
/// phone : "089689008988"
/// receiver_name : "Bapak Budi"
/// default : 1
/// city : "Cirebon"
/// province : "Provinsi"
/// address : "Alamat"
/// postal_code : "45192"
/// user_id : 1
/// created_at : "2021-04-19T03:54:39.000000Z"
/// updated_at : "2021-04-19T03:54:39.000000Z"

class AddressModel {
  int _id;
  String _name;
  String _phone;
  String _receiverName;
  bool _default;
  String _city;
  String _province;
  String _address;
  String _postalCode;
  int _userId;
  String _createdAt;
  String _updatedAt;

  int get id => _id;
  String get name => _name;
  String get phone => _phone;
  String get receiverName => _receiverName;
  bool get isDefault => _default;
  String get city => _city;
  String get province => _province;
  String get address => _address;
  String get postalCode => _postalCode;
  int get userId => _userId;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  AddressModel({
      int id, 
      String name, 
      String phone, 
      String receiverName, 
      bool isdefault,
      String city, 
      String province, 
      String address, 
      String postalCode, 
      int userId, 
      String createdAt, 
      String updatedAt}){
    _id = id;
    _name = name;
    _phone = phone;
    _receiverName = receiverName;
    _default = isDefault;
    _city = city;
    _province = province;
    _address = address;
    _postalCode = postalCode;
    _userId = userId;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  AddressModel.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _phone = json["phone"];
    _receiverName = json["receiver_name"];
    _default = json["default"];
    _city = json["city"];
    _province = json["province"];
    _address = json["address"];
    _postalCode = json["postal_code"];
    _userId = json["user_id"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["phone"] = _phone;
    map["receiver_name"] = _receiverName;
    map["default"] = _default;
    map["city"] = _city;
    map["province"] = _province;
    map["address"] = _address;
    map["postal_code"] = _postalCode;
    map["user_id"] = _userId;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    return map;
  }

}