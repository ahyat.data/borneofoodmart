class User {
  int id;
  String name;
  String email;
  dynamic emailVerifiedAt;
  String role;
  String createdAt;
  String updatedAt;
  String profileUrl;
  bool hasAddress;

  User(
      {id,
      name,
      email,
      emailVerifiedAt,
      role,
      createdAt,
      updatedAt,
      profileUrl, hasAddress});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    role = json['role'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    profileUrl = json['profile_url'];
    hasAddress = json['has_address'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['email_verified_at'] = emailVerifiedAt;
    data['role'] = role;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['profile_url'] = profileUrl;
    return data;
  }
}
