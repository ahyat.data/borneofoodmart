import 'package:borneofood/app/data/models/address_model.dart';
import 'package:borneofood/app/data/models/delivery_model.dart';

/// id : 3
/// total_price : 10000
/// status : "NEW_ORDER"
/// invoice : "INV-2021-04-18-0002"
/// payment_token : ""
/// payment_url : ""
/// created_at : "2021-04-18T09:42:51.000000Z"
/// updated_at : "2021-04-18T09:42:51.000000Z"
/// user_id : 1
/// address_id : 1
/// items : [{"id":3,"qty":1,"price":10000,"name":"Ayam Bakar","photo_url":"https://backend.app/noimage.png"}]

class Transaction {
  int _id;
  int _totalPrice;
  String _status;
  String _invoice;
  String _paymentToken;
  String _paymentUrl;
  String _createdAt;
  String _updatedAt;
  int _userId;
  int _addressId;
  List<Items> _items;
  AddressModel _address;
  DeliveryModel _delivery;

  int get id => _id;
  int get totalPrice => _totalPrice;
  String get status => _status;
  String get invoice => _invoice;
  String get paymentToken => _paymentToken;
  String get paymentUrl => _paymentUrl;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  int get userId => _userId;
  int get addressId => _addressId;
  List<Items> get items => _items;
  AddressModel get address => _address;
  DeliveryModel get delivery => _delivery;

  Transaction({
    int id,
    int totalPrice,
    String status,
    String invoice,
    String paymentToken,
    String paymentUrl,
    String createdAt,
    String updatedAt,
    int userId,
    int addressId,
    List<Items> items,
    AddressModel address,
    DeliveryModel delivery,
  }){
    _id = id;
    _totalPrice = totalPrice;
    _status = status;
    _invoice = invoice;
    _paymentToken = paymentToken;
    _paymentUrl = paymentUrl;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
    _addressId = addressId;
    _items = items;
    _address = address;
    _delivery = delivery;
  }

  Transaction.fromJson(dynamic json) {
    _id = json["id"];
    _totalPrice = json["total_price"];
    _status = json["status"];
    _invoice = json["invoice"];
    _paymentToken = json["payment_token"];
    _paymentUrl = json["payment_url"];
    _createdAt = json["created_at"];
    _updatedAt = json["updated_at"];
    _userId = json["user_id"];
    _addressId = json["address_id"];
    if (json["items"] != null) {
      _items = [];
      json["items"].forEach((v) {
        _items.add(Items.fromJson(v));
      });
    }
    _address = json["address"] != null ? AddressModel.fromJson(json["address"]) : null;
    _delivery = json["delivery_method"] != null ? DeliveryModel.fromJson(json["delivery_method"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["total_price"] = _totalPrice;
    map["status"] = _status;
    map["invoice"] = _invoice;
    map["payment_token"] = _paymentToken;
    map["payment_url"] = _paymentUrl;
    map["created_at"] = _createdAt;
    map["updated_at"] = _updatedAt;
    map["user_id"] = _userId;
    map["address_id"] = _addressId;
    if (_items != null) {
      map["items"] = _items.map((v) => v.toJson()).toList();
    }
    if (_address != null) {
      map["address"] = _address.toJson();
    }
    if (_delivery != null) {
      map["address"] = _delivery.toJson();
    }
    return map;
  }

}

/// id : 3
/// qty : 1
/// price : 10000
/// name : "Ayam Bakar"
/// photo_url : "https://backend.app/noimage.png"

class Items {
  int _id;
  int _qty;
  int _price;
  String _name;
  String _photoUrl;

  int get id => _id;
  int get qty => _qty;
  int get price => _price;
  String get name => _name;
  String get photoUrl => _photoUrl;

  Items({
      int id,
      int qty,
      int price,
      String name,
      String photoUrl}){
    _id = id;
    _qty = qty;
    _price = price;
    _name = name;
    _photoUrl = photoUrl;
}

  Items.fromJson(dynamic json) {
    _id = json["id"];
    _qty = json["qty"];
    _price = json["price"];
    _name = json["name"];
    _photoUrl = json["photo_url"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["qty"] = _qty;
    map["price"] = _price;
    map["name"] = _name;
    map["photo_url"] = _photoUrl;
    return map;
  }

}