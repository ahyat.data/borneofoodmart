/// id : 1
/// name : "Antar Pagi Hari"
/// description : "Pemesanan sebelum Jam 9 Pagi"
/// price : 5000

class DeliveryModel {
  int _id;
  String _name;
  String _description;
  int _price;

  int get id => _id;
  String get name => _name;
  String get description => _description;
  int get price => _price;

  DeliveryModel({
      int id, 
      String name, 
      String description, 
      int price}){
    _id = id;
    _name = name;
    _description = description;
    _price = price;
}

  DeliveryModel.fromJson(dynamic json) {
    _id = json["id"];
    _name = json["name"];
    _description = json["description"];
    _price = json["price"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["name"] = _name;
    map["description"] = _description;
    map["price"] = _price;
    return map;
  }

}