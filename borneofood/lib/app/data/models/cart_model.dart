import 'package:borneofood/app/data/models/food_model.dart';

class Cart {
  Food food;
  int qty;
  double total;

  Cart({
    this.food,
    this.qty,
    this.total,
  });
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = food.name;
    data['quantity']= qty;
    data['price']= food.price;
    data["id"] = food.id;
    return data;
  }

}
