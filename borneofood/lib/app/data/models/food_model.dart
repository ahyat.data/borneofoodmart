class Food {
  int id;
  String name;
  int price;
  int qty;
  String photoUrl;
  dynamic promo;
  String description;
  String createdAt;
  String updatedAt;

  Food(
      {id,
      name,
      price,
      qty,
      photoUrl,
      promo,
      description,
      createdAt,
      updatedAt});

  Food.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    qty = json['qty'];
    photoUrl = json['photo_url'];
    promo = json['promo'];
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['price'] = price;
    data['qty'] = qty;
    data['photo_url'] = photoUrl;
    data['promo'] = promo;
    data['description'] = description;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
