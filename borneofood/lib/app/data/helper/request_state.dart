import 'package:equatable/equatable.dart';

class RequestState extends Equatable {
  const RequestState();

  @override
  List<Object> get props => [];
}

class RequestLoadingState extends RequestState {}

class RequestLoadingDoneState extends RequestState {}
