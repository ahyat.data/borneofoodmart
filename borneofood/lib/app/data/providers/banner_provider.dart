import 'dart:io';

import 'package:borneofood/app/data/models/banner_model.dart';
import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:get/get.dart';

class BannerProvider extends BaseProvider {
  Future<Response> getAll() async {
    return await get('banners');
  }

  Future<Response> createBanner({File photo, String name}) async {
    final formData = FormData({
      "name": name,
    });
    if (photo != null) {
      final fileName = photo.path.split('/').last;
      formData
        ..files
            .add(MapEntry("photo", MultipartFile(photo, filename: fileName)));
    }
    return await post("banners", formData, headers: {"require_token": "true"});
  }

  Future<Response> editBanner(
      {File photo, BannerModel banner, String name}) async {
    final formData = FormData({
      "name": name,
      "_method": "PUT",
    });
    if (photo != null) {
      final fileName = photo.path.split('/').last;
      formData
        ..files
            .add(MapEntry("photo", MultipartFile(photo, filename: fileName)));
    }
    return await post("banners/${banner.id}", formData,
        headers: {"require_token": "true"});
  }

  Future<Response> deleteBanner({int bannerId}) async {
    return await delete("banners/$bannerId",
        headers: {"require_token": "true"});
  }

  updateBanner({String name, File photo}) {}
}
