import 'package:borneofood/app/data/models/delivery_model.dart';
import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:get/get_connect/http/src/response/response.dart';

class DeliveryProvider extends BaseProvider {
  Future<Response> getAll() async {
    return await get('deliveries');
  }

  Future<Response> createDelivery({
    String name,
    String description,
    double price,
  }) async {
    return await post('deliveries', {
      "name": name,
      "description": description,
      "price": price,
    }, headers: {
      "require_token": "true"
    });
  }

  Future<Response> updateDelivery({
    String name,
    String description,
    double price,
    DeliveryModel deliveryModel
  }) async {
    return await post('deliveries/${deliveryModel.id}', {
      "name": name,
      "description": description,
      "price": price,
      "_method": "PUT"
    }, headers: {
      "require_token": "true"
    });
  }


  Future<Response> deleteDelivery({
    DeliveryModel deliveryModel
  }) async {
    return await delete('deliveries/${deliveryModel.id}', headers: {
      "require_token": "true"
    });
  }
}
