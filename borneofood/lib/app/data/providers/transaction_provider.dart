import 'package:borneofood/app/data/models/cart_model.dart';
import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:get/get_connect/http/src/response/response.dart';

class TransactionProvider extends BaseProvider {
  Future<Response> getAll({String status}) async {
    String path = "transactions";
    print("Get All");
    Map<String, dynamic> queries = Map<String, dynamic>();
    if (status != null) {
      queries.addAll({"status": status});
    }

    final response =
        await get(path, query: queries, headers: {"require_token": "true"});
    print(response.request.url);
    return response;
  }

  Future<Response> createTransaction({
    List<Cart> carts,
    int addressId,
    int deliveryId,
    double totalPrice,
  }) async {
    return await post("transactions", {
      "address_id": addressId,
      "delivery_method_id": deliveryId,
      "total_price": totalPrice,
      "items": carts,
    }, headers: {
      'require_token': 'true'
    });
  }

  Future<Response> createGuestTransaction({
    List<Cart> carts,
    int deliveryId,
    double totalPrice,
    String name,
    String email,
    String city,
    String postalCode,
    String address,
    String phone,
    String province,
  }) async {
    return await post(
      "transactions/guest",
      {
        "delivery_method_id": deliveryId,
        "total_price": totalPrice,
        "items": carts,
        "name": name,
        "address": address,
        "phone": phone,
        "email": email,
        "city": city,
        "province": province,
        "postal_code": postalCode,
      },
    );
  }

  Future<Response> cancelTransaction(int id) async {
    return await get("transactions/cancel/$id",
        headers: {"require_token": "true"});
  }

  doneTransaction(int id) async {
    return await post("transactions/$id", {
      "status": "DONE",
      "_method": "PUT",
    }, headers: {
      'require_token': 'true'
    });
  }

  Future<Response> updateStatus({String status, int transactionId}) async {
    return await post("transactions/$transactionId", {
      "status": status,
      "_method": "PUT",
    }, headers: {
      'require_token': 'true'
    });
  }
}
