import 'dart:io';
import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthProvider extends BaseProvider {
  Future<Response> login(
    String email,
    String password,
  ) async {
    return await post("login", {
      "email": email,
      "password": password
    }, headers: {
      "Content-Type": "application/json",
    });
  }

  Future<Response> register({
    @required String email,
    @required String name,
    @required String password,
    @required String passwordConfirmation,
    File file,
  }) async {
    final formData = FormData({
      "email": email,
      "password": password,
      "password_confirmation": passwordConfirmation,
      "name": name,
      "role": "user",
    });
    if (file != null) {
      String fileName = file.path.split('/').last;
      formData
        ..files.add(MapEntry("photo", MultipartFile(file, filename: fileName)));
    }
    return await post("register", formData);
  }

  Future<Response> fetch() async {
    return await get("user", headers: {
      "require_token": "true",
    });
  }

  Future<Response> editUser(int id, String name, String email,
      {File file}) async {
    final formData = FormData({"name": name, "email": email});
    if (file != null) {
      String fileName = file.path.split('/').last;
      formData
        ..files.add(MapEntry("photo", MultipartFile(file, filename: fileName)));
    }
    return await post("user", formData, headers: {"require_token": "true"});
  }

  Future<Response> updatePassword(
      {String current, String newPass, String repeatPass}) async {
    return await post('user/changePass', {
      'current_password': current,
      'password': newPass,
      'password_confirmation': repeatPass
    }, headers: {
      "require_token": "true"
    });
  }
}
