import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:get/get.dart';

import '../models/user_model.dart';

class UserProvider extends BaseProvider {
  Future<Response<User>> getUser(int id) async => await get('users/$id');
  Future<Response<User>> postUser(User user) async => await post('users', User);
  Future<Response> deleteUser(int id) async => await delete('users/$id');
}
