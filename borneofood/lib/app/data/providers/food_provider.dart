import 'dart:io';
import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:get/get.dart';
import '../models/food_model.dart';

class FoodProvider extends BaseProvider {
  final AuthController authController = Get.find();
  Future<Response<List<Food>>> getAll({bool promo, bool stock}) async {
    String url = "foods";
    Map<String, dynamic> queries = Map<String, dynamic>();
    if (promo != null) {
      if (promo) {
        queries.addAll({"promo": promo.toString()});
      }
    }
    if (stock != null) {
      queries.addAll({"stock": stock.toString()});
    }
    final response = await get(url, query: queries);
    print(response.request.url);
    if (response.isOk) {
      List<Food> data;
      if (authController.authState.value is AuthenticationUnAuthed) {
        data = (response.body["data"] as Iterable).map((e) {
          e["price"] = e["price"] + 2000;
          Food.fromJson(e);
        }).toList();
      }
      data = (response.body["data"] as Iterable)
          .map((e) => Food.fromJson(e))
          .toList();
      print(data);
      return Response(body: data);
    } else {
      return Response(body: null, statusText: "Failed");
    }
  }

  Future<Response<List<Food>>> getByQuery(
      {String name = "",
      int limit = 10,
      int categoryId,
      bool promo,
      bool stock}) async {
    final url = "foods";
    Map<String, dynamic> queries = Map<String, dynamic>();
    queries.addAll({"limit": "$limit"});
    if (name != null) {
      queries.addAll({"name": name});
    }
    if (categoryId != null) {
      queries.addAll({"category": "$categoryId"});
    }
    if (promo != null) {
      queries.addAll({"promo": promo.toString()});
    }
    if (stock != null) {
      queries.addAll({"stock": stock.toString()});
    }
    final response = await get(url, query: queries);
    if (response.isOk) {
      List<Food> data;
      if (authController.authState.value is AuthenticationUnAuthed) {
        data = (response.body["data"] as Iterable).map((e) {
          e["price"] = e["price"] + 2000;
          Food.fromJson(e);
        }).toList();
      }
      data = (response.body["data"] as Iterable)
          .map((e) => Food.fromJson(e))
          .toList();
      return Response(body: data);
    } else {
      return Response(body: null);
    }
  }

  Future<Response> createFood({
    String name,
    String price,
    String qty,
    String desc,
    bool isPromo,
    int categoryId,
    File file,
  }) async {
    final formData = FormData({
      "name": name,
      "price": double.parse(price),
      "qty": int.parse(qty),
      "description": desc,
      "promo": isPromo ? 1 : 0,
      "category_id": categoryId,
    });
    if (file != null) {
      final fileName = file.path.split('/').last;
      formData
        ..files.add(MapEntry("photo", MultipartFile(file, filename: fileName)));
    }
    final response = await post("foods", formData, headers: {
      "require_token": "true",
    });
    return response;
  }

  Future<Response> updateFood({
    String name,
    String price,
    String desc,
    String qty,
    bool isPromo,
    Food food,
    File file,
    int categoryId,
  }) async {
    final formData = FormData({
      "name": name,
      "price": double.parse(price),
      "qty": int.parse(qty),
      "description": desc,
      "promo": isPromo ? 1 : 0,
      "category_id": categoryId,
      "_method": "PUT",
    });
    if (file != null) {
      final fileName = file.path.split('/').last;
      formData
        ..files.add(MapEntry("photo", MultipartFile(file, filename: fileName)));
    }
    final response = await post("foods/${food.id}", formData, headers: {
      "require_token": "true",
    });
    return response;
  }

  Future<Response> deleteFood(Food food) async {
    return await delete("foods/${food.id}", headers: {
      "Accept": "application/json",
      "require_token": "true",
    });
  }
}
