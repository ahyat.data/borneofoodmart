import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:get/get.dart';

import '../models/category_model.dart';

class CategoryProvider extends BaseProvider {
  Future<Response<List<Category>>> getAll() async {
    final response = await get("categories");
    if (response.isOk && response.body != null) {
      List<Category> data =
          (response.body["data"] as Iterable).map((e) => Category.fromJson(e)).toList();
      return Response(body: data);
    } else {
      return null;
    }
  }

  Future<Response<Category>> createCategory(String name) async {
    final response = await post("categories", {"name": name});
    if (response.isOk) {
      return Response(body: Category.fromJson(response.body["data"]));
    } else {
      return Response(body: null);
    }
  }
}
