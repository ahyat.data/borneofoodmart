import 'package:borneofood/app/data/providers/base_provider.dart';
import 'package:get/get_connect/http/src/response/response.dart';

class AddressProvider extends BaseProvider {
  Future<Response> createAddress({
    String nameAddress,
    String receiverName,
    String phone,
    String city,
    String province,
    String address,
    String kodePos, bool isDefault,
  }) async {
    return await post('address', {
      'name': nameAddress,
      'receiver_name': receiverName,
      'phone': phone,
      'city': city,
      'default': isDefault,
      'province': province,
      'address': address,
      'postal_code': kodePos,
    }, headers: {
      'require_token': "true"
    });
  }

  Future<Response> getAll() async {
    return await get('address', headers: {"require_token": "true"});
  }

  Future<Response> updateAddress(
      {String nameAddress,
      String receiverName,
      String phone,
      String city,
      String province,
      String address,
      String kodePos,
      int addressId, bool isDefault}) async {
    return await post('address/$addressId', {
      'name': nameAddress,
      'receiver_name': receiverName,
      'phone': phone,
      'city': city,
      'default': isDefault,
      'province': province,
      'address': address,
      'postal_code': kodePos,
      "_method": "PUT"
    }, headers: {
      'require_token': "true"
    });
  }
}
