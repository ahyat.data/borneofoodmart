import 'package:borneofood/app/constants/constants.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BaseProvider extends GetConnect {
  final box = GetStorage();

  @override
  void onInit() {
    super.onInit();
    final token = box.read('api_token');
    httpClient.baseUrl = BASE_API;
    httpClient.addRequestModifier((request) {
      request.headers.addAll({"Accept": "application/json"});
      if (request.headers.containsKey('require_token')) {
        request.headers.remove('require_token');
        request.headers.addAll(
          {'Authorization': 'Bearer $token'},
        );
      }
      return request;
    });
    httpClient.timeout = Duration(seconds: 5);
  }
}
