import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

final appBarTheme = AppBarTheme(
  centerTitle: true,
  backgroundColor: mainColor,
  titleTextStyle: mainText,
  toolbarTextStyle: mainText,
  iconTheme: IconThemeData(
    color: Colors.white,
  ),
  brightness: Brightness.dark,
  systemOverlayStyle: SystemUiOverlayStyle(
    statusBarBrightness: Brightness.dark,
    systemNavigationBarColor: Colors.transparent,
  ),
);

final bottomAppbarTheme = BottomAppBarTheme(
  color: mainColor,
);

final appTheme = ThemeData(
  backgroundColor: mainColor,
  appBarTheme: appBarTheme,
  primaryColor: mainColor,
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: mainText.copyWith(
      fontSize: 14.0,
    ),
  ),
  bottomAppBarTheme: bottomAppbarTheme,
  bottomAppBarColor: mainColor,
);
