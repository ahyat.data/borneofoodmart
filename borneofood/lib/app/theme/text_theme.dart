import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const mainText = TextStyle(
  fontFamily: 'ProductSans',
  letterSpacing: 0.5,
);
String formatCurrency(num amount, {int decimalCount = 0}) {
  final formatCurrency = new NumberFormat.simpleCurrency(
    decimalDigits: decimalCount,
    locale: 'id-ID',
    name: 'Rp ',
  );
  return formatCurrency.format(amount);
}
