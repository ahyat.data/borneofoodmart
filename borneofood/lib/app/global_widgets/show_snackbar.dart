import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showSuccessMessage(String message) {
  return Get.snackbar(
    "",
    "",
    backgroundColor: Colors.greenAccent[400],
    titleText: Text(
      "Informasi Sukses",
      style: mainText.copyWith(color: Colors.white),
    ),
    messageText: Text(
      message,
      style: mainText.copyWith(color: Colors.white),
    ),
    snackStyle: SnackStyle.GROUNDED,
    margin: EdgeInsets.all(0),
  );
}

void showErrorMessage(String message) {
  return Get.snackbar(
    "",
    "",
    backgroundColor: Colors.redAccent,
    titleText: Text(
      "Error Informasi",
      style: mainText.copyWith(color: Colors.white),
    ),
    messageText: Text(
      message,
      style: mainText.copyWith(color: Colors.white),
    ),
    snackStyle: SnackStyle.GROUNDED,
    margin: EdgeInsets.all(0),
  );
}
