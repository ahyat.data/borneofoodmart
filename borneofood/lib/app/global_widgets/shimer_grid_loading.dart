import 'package:flutter/material.dart';
import 'package:skeleton_loader/skeleton_loader.dart';

class ShimmerGridLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        SkeletonGridLoader(
          builder: Card(
            color: Colors.transparent,
            child: GridTile(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 70,
                    color: Colors.white,
                  ),
                  SizedBox(height: 10),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.0),
                    height: 20,
                    color: Colors.white,
                  ),
                  SizedBox(height: 10),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.0),
                    height: 20,
                    color: Colors.white,
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
          items: 8,
          itemsPerRow: 2,
          period: Duration(seconds: 2),
          highlightColor: Colors.grey[100],
          direction: SkeletonDirection.ltr,
          childAspectRatio: 1,
        ),
      ],
    );
  }
}
