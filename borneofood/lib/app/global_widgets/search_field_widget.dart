import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class SearchFieldWidget extends StatelessWidget {
  final Function(String query) onSearch;
  final String placeHolder;
  final TextEditingController controller;

  const SearchFieldWidget({Key key, this.onSearch, this.placeHolder, this.controller, })
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextField(
      style: mainText.copyWith(fontSize: 14),
      controller: controller,
      onSubmitted: onSearch,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(vertical: 4.0),
        fillColor: Colors.white,
        filled: true,
        prefixIcon: Icon(Icons.search, color: mainColor,),
        border: OutlineInputBorder(
            borderSide: BorderSide(
          color: mainColor,
        )),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
          color: mainColor,
        )),
        enabledBorder:OutlineInputBorder(
        borderSide: BorderSide(
        color: mainColor,
      ),),
        hintStyle: mainText.copyWith(fontWeight: FontWeight.w300, fontSize: 12),
        hintText: placeHolder,
      ),
    );
  }
}
