import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final Function onClick;
  final IconData icon;
  final Color bgcolor;
  final Color iconcolor;

  const CircleButton(
      {Key key, this.onClick, this.icon, this.bgcolor, this.iconcolor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: bgcolor,
        ),
        child: Icon(
          icon,
          color: iconcolor,
        ),
      ),
    );
  }
}
