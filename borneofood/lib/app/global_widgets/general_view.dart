import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class GeneralView extends StatelessWidget {
  final String title;
  final Widget body;
  final Widget bottomWidget;
  final bool isScrollable;

  final Widget  floatingWidget;

  const GeneralView({Key key, this.title, this.body, this.isScrollable=true, this.bottomWidget, this.floatingWidget}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0),
          ),
        ),
        title: Text(
          title,
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: ListView(
        physics: this.isScrollable ?  null : const NeverScrollableScrollPhysics(),
        children: [
          body ?? SizedBox(),
        ],
      ),
      bottomNavigationBar: bottomWidget ?? SizedBox(),
      floatingActionButton: floatingWidget ?? SizedBox(),
    );
  }
}
