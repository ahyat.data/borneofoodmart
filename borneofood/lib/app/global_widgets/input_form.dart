import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputForm extends StatelessWidget {
  final String name;
  final String hintText;
  final controller;
  final bool obscureText;
  final bool currency;
  final TextInputType type;
  final int minLine;
  final int maxLine;

  const InputForm({
    Key key,
    this.name,
    this.controller,
    this.hintText,
    this.type = TextInputType.text,
    this.obscureText = false,
    this.minLine = 1,
    this.maxLine = 1,
    this.currency = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: mainText.copyWith(
              color: Colors.black87,
              fontSize: 14.0,
            ),
          ),
          SizedBox(
            height: 4.0,
          ),
          TextField(
            style: mainText.copyWith(
              fontSize: 14,
            ),
            controller: controller,
            minLines: minLine,
            maxLines: maxLine,
            keyboardType: type,
            obscureText: obscureText,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: mainColor),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: mainColor),
              ),
              hintText: hintText,
              // isDense: true,
              hintStyle: mainText.copyWith(
                color: Colors.black26,
                fontWeight: FontWeight.normal,
                fontSize: 14.0,
              ),
            ),
          )
        ],
      ),
    );
  }
}
