import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final Color bgColor;
  final Color textColor;
  final String text;
  final Function onClick;

  const ActionButton({Key key, this.bgColor=mainColor, this.textColor=Colors.white, this.text, this.onClick}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ElevatedButton(
        child: Text(text),
        style: ElevatedButton.styleFrom(
          primary: bgColor,
          textStyle: mainText.copyWith(
            color: bgColor,
          )
        ),
        onPressed: onClick,
      ),
    );
  }
}
