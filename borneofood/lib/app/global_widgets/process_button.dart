import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class ProcessButton extends StatelessWidget {
  final String title;
  final bool outline;
  final Function onClick;

  const ProcessButton({
    Key key,
    this.title,
    this.onClick,
    this.outline = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50.0,
        width: double.infinity,
        margin: EdgeInsets.only(top: 24.0),
        child: outline
            ? OutlinedButton(
                onPressed: onClick,
          child: Text(title),
          style: OutlinedButton.styleFrom(
            backgroundColor: Colors.white,
            textStyle: mainText.copyWith(
              color: mainColor,
            ),
            primary: mainColor,
            side: BorderSide(
              color: mainColor,
            )
          ),
              )
            : ElevatedButton(
                onPressed: onClick,
                style: ElevatedButton.styleFrom(
                  primary: outline ? Colors.white : mainColor,
                  textStyle: mainText.copyWith(
                    color: mainColor,
                  ),
                ),
                child: Text(title),
              ));
  }
}
