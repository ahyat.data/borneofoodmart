import 'package:borneofood/app/routes/app_pages.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void openShopingPage() {
    Get.toNamed(Routes.DASHBOARD);
  }

  void openLoginPage() {
    Get.toNamed(Routes.LOGIN);
  }
}
