import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: RichText(
          text: TextSpan(
              text: "Borneofood",
              style: mainText.copyWith(
                color: Colors.white,
                fontSize: 15.0,
              ),
              children: [
                TextSpan(
                  text: "Mart",
                  style: mainText.copyWith(
                    color: Colors.yellowAccent[400],
                    fontSize: 15.0,
                  ),
                )
              ]),
        ),
      ),
      body: Container(
        height: Get.height,
        margin: EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset(
              'assets/logo.png',
              height: 120,
              width: 120,
            ),
            Text.rich(
              TextSpan(
                text: "Your Best Supplier",
                style: mainText.copyWith(
                  color: mainColor,
                ),
              ),
            ),
            Text.rich(
              TextSpan(
                text: "in",
                style: mainText.copyWith(
                  color: mainColor,
                ),
                children: [
                  TextSpan(
                    text: " Food Service",
                    style: mainText.copyWith(
                      fontSize: 22.0,
                      fontWeight: FontWeight.w700,
                    ),
                  )
                ],
              ),
            ),
            Expanded(child: SizedBox()),
            ProcessButton(
              title: "Langsung Belanja",
              outline: true,
              onClick: () {
                controller.openShopingPage();
              },
            ),
            ProcessButton(
              title: "Login",
              onClick: controller.openLoginPage,
            ),
            SizedBox(height: 32,),
          ],
        ),
      ),
    );
  }
}
