import 'package:borneofood/app/data/models/address_model.dart';
import 'package:borneofood/app/data/providers/address_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/add_edit_address_view.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/order_view.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:get/get.dart';

class ProfileController extends GetxController {
  final isAddressLoading = false.obs;
  final AddressProvider addressProvider = Get.put(AddressProvider());
  RxList<AddressModel> addresses = RxList<AddressModel>();

  @override
  void onInit() {
    super.onInit();
    getAllAddresses();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void openListOrder() {
    Get.to(() => OrderView());
  }

  Future<void> saveFirstAddress({
    String nameAddress,
    String receiverName,
    String phone,
    String city,
    String province,
    String address,
    String kodePos,
  }) async {
    showLoadingWidget();
    final response = await addressProvider.createAddress(
      nameAddress: nameAddress,
      receiverName: receiverName,
      phone: phone,
      city: city,
      province: province,
      address: address,
      kodePos: kodePos,
      isDefault: true,
    );
    closeLoadingWidget();
    if (response.isOk) {
      showSuccessMessage(response.body["meta"]["message"]);
      await Future.delayed(Duration(seconds: 1));
      Get.offAllNamed(Routes.DASHBOARD);
    } else {
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  Future<void> saveCreateAddress({
    String nameAddress,
    String receiverName,
    String phone,
    String city,
    String province,
    String address,
    String kodePos,
    bool isDefault,
  }) async {
    showLoadingWidget();
    final response = await addressProvider.createAddress(
        nameAddress: nameAddress,
        receiverName: receiverName,
        phone: phone,
        city: city,
        province: province,
        address: address,
        kodePos: kodePos,
        isDefault: isDefault);
    closeLoadingWidget();
    if (response.isOk) {
      showSuccessMessage(response.body["meta"]["message"]);
      await Future.delayed(Duration(seconds: 1));
      Get.back();
    } else {
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  void getAllAddresses() async {
    isAddressLoading(true);
    final response = await addressProvider.getAll();
    isAddressLoading(false);
    if (response.isOk) {
      final data = (response.body["data"] as Iterable)
          .map((e) => AddressModel.fromJson(e))
          .toList();
      addresses(data);
    }
  }

  void updateAddress({AddressModel address}) {
    Get.to(() => AddEditAddressView(address));
  }

  void deleteAddress({AddressModel address}) async {
    showLoadingWidget();
    final response = await addressProvider
        .delete("address/${address.id}", headers: {"require_token": "true"});
    closeLoadingWidget();
    if (response.isOk) {
      showSuccessMessage("Alamat Berhasil Di Hapus");
      getAllAddresses();
    } else {
      showErrorMessage("Terjadi Masalah Ketika Menghapus Data");
      getAllAddresses();
    }
  }

  Future<void> simpanUpdateAddress({
    String nameAddress,
    String receiverName,
    String phone,
    String city,
    String province,
    String address,
    String kodePos,
    int addressId,
    bool isDefault,
  }) async {
    showLoadingWidget();
    final response = await addressProvider.updateAddress(
      nameAddress: nameAddress,
      receiverName: receiverName,
      phone: phone,
      city: city,
      province: province,
      address: address,
      kodePos: kodePos,
      isDefault: isDefault,
      addressId: addressId,
    );
    closeLoadingWidget();
    if (response.isOk) {
      showSuccessMessage(response.body["meta"]["message"]);
      await Future.delayed(
        Duration(
          seconds: 1,
        ),
      );
      Get.back();
    } else {
      print(response.body);
      showErrorMessage(response.body["meta"]["message"]);
    }
  }
}
