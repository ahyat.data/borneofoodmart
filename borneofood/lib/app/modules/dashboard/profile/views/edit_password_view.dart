import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class EditPasswordView extends GetView {
  final AuthController controller= Get.find();
  final currentPassword = TextEditingController();
  final newPassword = TextEditingController();
  final repeatPassword = TextEditingController();
  final User user;
  EditPasswordView(this.user);

  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Ubah Password",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          children: [
            InputForm(
              name: "Password Lama",
              hintText: "Masukan Password sekarang",
              type: TextInputType.text,
              obscureText: true,
              controller: currentPassword,
            ),
            InputForm(

              name: "Password Baru",
              hintText: "Password Baru",
              type: TextInputType.text,
              obscureText: true,
              controller: newPassword,
            ),
            InputForm(
              controller: repeatPassword,
              name: "Ulangi Password",
              hintText: "Ulangi Password",
              type: TextInputType.visiblePassword,
              obscureText: true,
            ),

            ProcessButton(
              title: "Simpan",
              outline: false,
              onClick: () async {
                await controller.updatePassword(
                  current: currentPassword.text,
                  newPass: newPassword.text,
                  repeatPass: repeatPassword.text
                );
                currentPassword.clear();
                newPassword.clear();
                repeatPassword.clear();
              },
            ),
          ],
        ),
      ),
    );
  }
}
