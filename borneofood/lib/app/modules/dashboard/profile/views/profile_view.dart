import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/menu_button.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/edit_password_view.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/profile_controller.dart';
import 'editprofile_view.dart';

class ProfileView extends StatelessWidget {
  final controller = Get.put(ProfileController());
  final AuthController authController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        bool isLogin = (authController.authState.value is AuthenticationAuthed);
        final state = authController.authState.value;
        return GeneralView(
          title: isLogin
              ? (state as AuthenticationAuthed).user.name
              : "Anda Belum terdaftar",
          body: Column(
            children: [
              buildWidget(state),
            ],
          ),
        );
      },
    );
  }

  buildWidget(AuthenticationState authState) {
    if (authState is AuthenticationAuthed) {
      final user = authState.user;
      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            Container(
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFF0F0F0),
                  image: DecorationImage(
                    image: NetworkImage(user.profileUrl),
                    fit: BoxFit.cover,
                  )),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              user.email,
              style: mainText,
            ),
            SizedBox(
              height: 24,
            ),
            MenuButton(
              icon: Icons.list_alt,
              text: "List Order",
              onPressed: () {
                controller.openListOrder();
              },
            ),
            MenuButton(
              icon: Icons.chat_outlined,
              text: "Faq",
              onPressed: () {
                Get.toNamed(Routes.FAQ);
              },
            ),
            MenuButton(
              icon: Icons.person,
              text: "Change Profile",
              onPressed: () {
                Get.to(() => EditProfileView(user));
              },
            ),
            MenuButton(
              icon: Icons.vpn_key,
              text: "Change Password",
              onPressed: () {
                Get.to(() => EditPasswordView(user));
              },
            ),
            MenuButton(
              icon: Icons.logout,
              text: "Logout",
              onPressed: () {
                authController.logout();
              },
            ),
          ],
        ),
      );
    }
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Image.asset(
            'assets/logo.png',
            height: 120,
          ),
          SizedBox(
            height: 16.0,
          ),
          MenuButton(
            text: "Daftar Dan Belanja Sekarang",
            icon: Icons.app_registration,
            onPressed: () {
              Get.toNamed(Routes.REGISTER);
            },
          ),
          MenuButton(
            text: "Faq",
            icon: Icons.chat_outlined,
            onPressed: () {
              Get.toNamed(Routes.FAQ);
            },
          ),
          MenuButton(
            text: "Login",
            icon: Icons.login,
            onPressed: () {
              Get.offNamed(Routes.LOGIN);
            },
          ),
          SizedBox(
            height: 16.0,
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            width: double.infinity,
            color: Colors.grey[200],
            child: RichText(
              text: TextSpan(
                  text: "Keuntungan Menjadi Member\n\n"
                      "1. Dapatkan Potongan harga Rp.2000\n"
                      "2. Dapatkan Potongan harga untuk event tertentu",
                  style: mainText.copyWith(
                    color: Colors.black,
                  )),
            ),
          )
        ],
      ),
    );
  }
}
