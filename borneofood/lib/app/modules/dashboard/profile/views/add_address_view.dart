import 'package:borneofood/app/data/models/address_model.dart';
import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/global_widgets/shimmer_list_loader.dart';
import 'package:borneofood/app/modules/dashboard/profile/controllers/profile_controller.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/add_edit_address_view.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddAddressView extends GetView {
  final ProfileController controller = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(16.0),
            bottomRight: Radius.circular(16.0),
          ),
        ),
        title: Text(
          "List Alamat",
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: RefreshIndicator(
                onRefresh: () async {
                  controller.getAllAddresses();
                },
                child: Obx(
                  () => (controller.isAddressLoading.value)
                      ? ListView(
                          children: [
                            ShimmerListLoading(),
                          ],
                        )
                      : ListView.builder(
                          itemCount: controller.addresses.length,
                          itemBuilder: (ctx, index) {
                            AddressModel address = controller.addresses[index];
                            return AddressCard(
                              address: address,
                              controller: controller,
                            );
                          },
                        ),
                ),
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Get.to(() => AddEditAddressView(null));
        },
        backgroundColor: mainColor,
      ),
    );
  }
}

class AddressCard extends StatelessWidget {
  final AddressModel address;
  final ProfileController controller;

  const AddressCard({Key key, this.address, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: Card(
        child: Container(
          margin: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                address.name,
                style: mainText,
              ),
              Divider(
                thickness: 1,
              ),
              Table(
                children: [
                  TableRow(children: [
                    Text(
                      "Nama : ",
                      style: mainText,
                    ),
                    Text("${address.receiverName}", style: mainText)
                  ]),
                  TableRow(children: [
                    Text("No Hp :", style: mainText),
                    Text("${address.phone}", style: mainText)
                  ]),
                  TableRow(children: [
                    Text("Kode Pos : ", style: mainText),
                    Text("${address.postalCode}", style: mainText)
                  ]),
                  TableRow(children: [
                    Text("Alamat Lengkap :", style: mainText),
                    Text("${address.address}", style: mainText)
                  ]),
                  TableRow(children: [
                    Text("Kota Asal :", style: mainText),
                    Text("${address.city}", style: mainText)
                  ]),
                ],
              ),
              Divider(
                thickness: 1,
              ),
              Row(
                children: [
                  ActionButton(
                    onClick: () {
                      controller.updateAddress(address: address);
                    },
                    text: "Edit",
                    bgColor: Colors.green,
                  ),
                  address.isDefault
                      ? SizedBox()
                      : ActionButton(
                          onClick: () {
                            controller.deleteAddress(address:address);
                          },
                          text: "Hapus",
                          bgColor: Colors.red,
                        ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
