import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/edit_password_view.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EditProfileView extends StatelessWidget {
  final AuthController controller = Get.find();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final User user;

  EditProfileView(this.user);
  @override
  Widget build(BuildContext context) {
    if(user != null){
     nameController.text = user.name;
      emailController.text = user.email;
    }
    return GeneralView(
      title: "Edit Profile",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                controller.selectAvatarFile();
              },
              child: GetBuilder(
                init: controller,
                builder:(value){
                  return  Container(
                    height: 120,
                    width: 120,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        DottedBorder(
                          child: Container(
                            width: 110,
                            height: 110,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFF0F0F0),
                                image: (controller.avatarFile == null)
                                    ? null
                                    : DecorationImage(
                                    image: FileImage(controller.avatarFile),
                                    fit: BoxFit.cover)),
                            child: (controller.avatarFile == null)
                                ? Center(
                                child: Text(
                                  "Upload\nPhoto",
                                  textAlign: TextAlign.center,
                                  style: mainText.copyWith(
                                    color: Color(0xFF8D92A3),
                                    fontSize: 12.0,
                                  ),
                                ))
                                : SizedBox(),
                          ),
                          padding: EdgeInsets.all(6),
                          borderType: BorderType.RRect,
                          radius: Radius.circular(60),
                          strokeWidth: 1,
                          color: Colors.grey,
                          dashPattern: [6,6],
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            InputForm(
              controller: nameController,
              name: "Nama Lengkap",
              hintText: "Nama Lengkap",
              type: TextInputType.text,
            ),
            InputForm(
              controller: emailController,
              name: "Alamat Email",
              hintText: "Alamat Email Aktif",
              type: TextInputType.emailAddress,
            ),

            ProcessButton(
              title: "Simpan",
              outline: false,
              onClick: () async {
                await controller.editUser(user, nameController.text, emailController.text);
                showSuccessMessage("User Berhasil Diperbarui");
              },
            ),
            ProcessButton(
              outline: true,
              title: "Ubah Password",
              onClick: (){
                Get.to(() => EditPasswordView(user));
              },
            ),
            (user.role == "admin") ? SizedBox() :
            ProcessButton(
              outline: true,
              title: "Ubah Alamat",
              onClick: (){
                controller.editAddress();
              },
            )
          ],
        ),
      ),
    );
  }
}
