import 'package:borneofood/app/data/models/transaction_model.dart';
import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/modules/admin/transaction/controllers/transaction_controller.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:whatsapp_unilink/whatsapp_unilink.dart';

class OrderView extends GetView {
  final TransactionController controller = Get.put(TransactionController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'List Transaksi',
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w200,
            // fontStyle: FontStyle.italic,
          ),
        ),
        bottom: TabBar(
          physics: NeverScrollableScrollPhysics(),
          controller: controller.tabController,
          isScrollable: true,
          labelStyle: mainText,
          onTap: (index) {
            controller.setIndex(index);
          },
          tabs: [
            Tab(
              text: "Menunggu Pembayaran",
            ),
            Tab(text: "Sedang Di Proses"),
            Tab(text: "Sedang Di Kirim"),
            Tab(text: "Selesai"),
            Tab(text: "Di Batalkan"),
          ],
        ),
      ),
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller.tabController,
        children: [
          Obx(
                () {
              return Container(
                margin: EdgeInsets.all(8.0),
                child: !controller.isLoadTransaction.value
                    ? controller.transactions.length > 0
                    ? ListView(
                  children: controller.transactions.map((element) {
                    return Card(
                      child: Container(
                        margin: EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  element.createdAt,
                                  overflow: TextOverflow.ellipsis,
                                  style: mainText.copyWith(),
                                ),
                                buildChip(element),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            ...element.items.map((e) {
                              return ListTile(
                                contentPadding: EdgeInsets.all(0),
                                dense: true,
                                leading: Image.network(
                                  e.photoUrl,
                                  width: 60,
                                  height: 40,
                                  fit: BoxFit.cover,
                                ),
                                title: Text(
                                  e.name,
                                  style: mainText.copyWith(
                                    fontSize: 14.0,
                                  ),
                                ),
                                subtitle: Text(
                                  "${e.qty.toString()} Buah",
                                  style: mainText,
                                ),
                              );
                            }).toList(),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Total Belanja",
                                        style: mainText.copyWith(
                                          fontSize: 12,
                                        ),
                                      ),
                                      Text(
                                        "${formatCurrency(element.totalPrice)}",
                                        style: mainText.copyWith(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                ActionButton(
                                  onClick: () {
                                    controller
                                        .bayarTransaction(element);
                                  },
                                  text: "Bayar",
                                  bgColor: Colors.green,
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                ActionButton(
                                  onClick: () {
                                    controller
                                        .cancelTransaction(element);
                                  },
                                  text: "Cancel",
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }).toList(),
                )
                    : Center(
                  child: Text("Tidak Ada Trasaksi", style: mainText,),
                )
                    : Center(
                  child: CircularProgressIndicator(),
                ),
              );
            },
          ),
          Obx(
            () {
              return Container(
                margin: EdgeInsets.all(8.0),
                child: !controller.isLoadTransaction.value
                    ? controller.transactions.length > 0
                    ? ListView(
                        children: controller.transactions.map((element) {
                          return Card(
                            child: Container(
                              margin: EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        element.createdAt,
                                        overflow: TextOverflow.ellipsis,
                                        style: mainText.copyWith(),
                                      ),
                                      buildChip(element),
                                    ],
                                  ),
                                  Divider(
                                    thickness: 1,
                                  ),
                                  ...element.items.map((e) {
                                    return ListTile(
                                      contentPadding: EdgeInsets.all(0),
                                      dense: true,
                                      leading: Image.network(
                                        e.photoUrl,
                                        width: 60,
                                        height: 40,
                                        fit: BoxFit.cover,
                                      ),
                                      title: Text(
                                        e.name,
                                        style: mainText.copyWith(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                      subtitle: Text(
                                        "${e.qty.toString()} Buah",
                                        style: mainText,
                                      ),
                                    );
                                  }).toList(),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Total Belanja",
                                              style: mainText.copyWith(
                                                fontSize: 12,
                                              ),
                                            ),
                                            Text(
                                              "${formatCurrency(element.totalPrice)}",
                                              style: mainText.copyWith(
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      ActionButton(
                                        onClick: () async{
                                          final link = WhatsAppUnilink(phoneNumber: "+6281352559997", text: "Hallo admin saya ingin menanyakan pesanan dengan no invoice: ${element.invoice}");
                                          await launch('$link');
                                        },
                                        text: "Tanya Penjual",
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        }).toList(),
                      ): Center(
                    child: Text("Tidak Ada Trasaksi", style: mainText,),)
                    :  Center(
                        child: CircularProgressIndicator(),
                      ),
              );
            },
          ),
          Obx(
            () {
              return Container(
                margin: EdgeInsets.all(8.0),
                child: !controller.isLoadTransaction.value
                    ? controller.transactions.length > 0
                        ? ListView(
                            children: controller.transactions.map((element) {
                              return Card(
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            element.createdAt,
                                            overflow: TextOverflow.ellipsis,
                                            style: mainText.copyWith(),
                                          ),
                                          buildChip(element),
                                        ],
                                      ),
                                      Divider(
                                        thickness: 1,
                                      ),
                                      ...element.items.map((e) {
                                        return ListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          dense: true,
                                          leading: Image.network(
                                            e.photoUrl,
                                            width: 60,
                                            height: 40,
                                            fit: BoxFit.cover,
                                          ),
                                          title: Text(
                                            e.name,
                                            style: mainText.copyWith(
                                              fontSize: 14.0,
                                            ),
                                          ),
                                          subtitle: Text(
                                            "${e.qty.toString()} Buah",
                                            style: mainText,
                                          ),
                                        );
                                      }).toList(),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Total Belanja",
                                                  style: mainText.copyWith(
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Text(
                                                  "${formatCurrency(element.totalPrice)}",
                                                  style: mainText.copyWith(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          ActionButton(
                                            onClick: () {
                                              controller.setTransactionDone(element);
                                            },
                                            text: "Selesai",
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }).toList(),
                          )
                        : Center(
                            child: Text("Tidak Ada Trasaksi", style: mainText,),
                          )
                    : Center(
                        child: CircularProgressIndicator(),
                      ),
              );
            },
          ),
          Obx(
            () {
              return Container(
                margin: EdgeInsets.all(8.0),
                child: !controller.isLoadTransaction.value
                    ? controller.transactions.length > 0
                        ? ListView(
                            children: controller.transactions.map((element) {
                              return Card(
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            element.createdAt,
                                            overflow: TextOverflow.ellipsis,
                                            style: mainText.copyWith(),
                                          ),
                                          buildChip(element),
                                        ],
                                      ),
                                      Divider(
                                        thickness: 1,
                                      ),
                                      ...element.items.map((e) {
                                        return ListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          dense: true,
                                          leading: Image.network(
                                            e.photoUrl,
                                            width: 60,
                                            height: 40,
                                            fit: BoxFit.cover,
                                          ),
                                          title: Text(
                                            e.name,
                                            style: mainText.copyWith(
                                              fontSize: 14.0,
                                            ),
                                          ),
                                          subtitle: Text(
                                            "${e.qty.toString()} Buah",
                                            style: mainText,
                                          ),
                                        );
                                      }).toList(),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Total Belanja",
                                                  style: mainText.copyWith(
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Text(
                                                  "${formatCurrency(element.totalPrice)}",
                                                  style: mainText.copyWith(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }).toList(),
                          )
                        : Center(
                            child: Text("Tidak Ada Trasaksi", style: mainText,),
                          )
                    : Center(
                        child: CircularProgressIndicator(),
                      ),
              );
            },
          ),
          Obx(
            () {
              return Container(
                margin: EdgeInsets.all(8.0),
                child: !controller.isLoadTransaction.value
                    ? controller.transactions.length > 0
                        ? ListView(
                            children: controller.transactions.map((element) {
                              return Card(
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            element.createdAt,
                                            overflow: TextOverflow.ellipsis,
                                            style: mainText.copyWith(),
                                          ),
                                          buildChip(element),
                                        ],
                                      ),
                                      Divider(
                                        thickness: 1,
                                      ),
                                      ...element.items.map((e) {
                                        return ListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          dense: true,
                                          leading: Image.network(
                                            e.photoUrl,
                                            width: 60,
                                            height: 40,
                                            fit: BoxFit.cover,
                                          ),
                                          title: Text(
                                            e.name,
                                            style: mainText.copyWith(
                                              fontSize: 14.0,
                                            ),
                                          ),
                                          subtitle: Text(
                                            "${e.qty.toString()} Buah",
                                            style: mainText,
                                          ),
                                        );
                                      }).toList(),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Total Belanja",
                                                  style: mainText.copyWith(
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Text(
                                                  "${formatCurrency(element.totalPrice)}",
                                                  style: mainText.copyWith(
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }).toList(),
                          )
                        : Center(
                            child: Text("Tidak Ada Trasaksi"),
                          )
                    : Center(
                        child: CircularProgressIndicator(),
                      ),
              );
            },
          ),
        ],
      ),
    );
  }

  Chip buildChip(Transaction element) {
    String text;
    Color bgColor, fgColor;
    switch (element.status) {
      case "NEW_ORDER":
        text = "Pesanan Baru";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "PENDING":
        text = "Belum Bayar";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "ON_PROCESS":
        text = "Sedang Diproses";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "CANCELLED":
        text = "Dibatalkan";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "DONE":
        text = "Transaksi Selesai";
        bgColor = Colors.green;
        fgColor = Colors.white;
        break;
      case "SENDING":
        text = "Sedang Dikirim";
        bgColor = Colors.green;
        fgColor = Colors.white;
        break;
    }
    return Chip(
      backgroundColor: bgColor,
      label: Text(
        text,
        style: mainText.copyWith(
          fontSize: 12,
          color: fgColor,
        ),
      ),
      padding: EdgeInsets.all(2.0),
    );
  }
}
