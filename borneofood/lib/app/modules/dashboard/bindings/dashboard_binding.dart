import 'package:borneofood/app/data/providers/banner_provider.dart';
import 'package:borneofood/app/data/providers/category_provider.dart';
import 'package:borneofood/app/data/providers/food_provider.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/modules/dashboard/info/controllers/info_controller.dart';
import 'package:borneofood/app/modules/dashboard/profile/controllers/profile_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/controllers/shoping_controller.dart';
import 'package:get/get.dart';

import '../controllers/dashboard_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(
      () => DashboardController(),
    );
    Get.lazyPut<ShopingController>(() => ShopingController());
    Get.lazyPut<CartController>(() => CartController());
    Get.lazyPut<InfoController>(() => InfoController());
    Get.lazyPut<ProfileController>(() => ProfileController());
    Get.lazyPut<BannerProvider>(() => BannerProvider());
    Get.lazyPut<FoodProvider>(() => FoodProvider());
    Get.lazyPut<CategoryProvider>(() => CategoryProvider());
  }
}
