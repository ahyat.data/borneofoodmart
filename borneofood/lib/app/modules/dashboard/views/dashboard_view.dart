import 'package:borneofood/app/modules/dashboard/cart/views/cart_view.dart';
import 'package:borneofood/app/modules/dashboard/info/views/info_view.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/profile_view.dart';
import 'package:borneofood/app/modules/dashboard/shoping/views/shoping_view.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/dashboard_controller.dart';

class DashboardView extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: controller.pageController,
        children: [
          ShopingView(),
          InfoView(),
          CartView(),
          ProfileView(),
        ],
      ),
      bottomNavigationBar: Obx(() => BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: mainColor,
            selectedItemColor: Colors.white,
            selectedLabelStyle: mainText,
            unselectedLabelStyle: mainText,
            unselectedItemColor: Colors.white54,
            elevation: 0,
            currentIndex: controller.indexPage.value,
            onTap: (index) {
              controller.setIndexPage(index);
            },
            // unselectedItemColor: Colors.white38,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.shopping_bag_outlined),
                label: "Belanja",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.info_outlined),
                label: "Info",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart_rounded),
                label: "Keranjang",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person_outline),
                label: "Akun",
              )
            ],
          )),
    );
  }
}
