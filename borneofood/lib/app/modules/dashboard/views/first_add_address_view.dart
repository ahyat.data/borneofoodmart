import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/modules/dashboard/profile/controllers/profile_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class FirstAddAddressView extends GetView {
  final ProfileController controller = Get.put(ProfileController());
  final nameAddressController = TextEditingController();
  final receiverNameController = TextEditingController();
  final phoneController = TextEditingController();
  final cityController = TextEditingController();
  final provinceController = TextEditingController();
  final addressController = TextEditingController();
  final postalCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Lengkapi Data Diri (Alamat)",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          children: [
            InputForm(
              name: "Nama Alamat",
              controller: nameAddressController,
              hintText: "Contoh: Rumah 1",
              type: TextInputType.text,
            ),
            InputForm(
              controller: receiverNameController,
              name: "Nama Penerima",
              hintText: "Contoh: Bapak Budi",
              type: TextInputType.text,
            ),
            InputForm(
              name: "Nomer Handphone",
              controller: phoneController,
              hintText: "Nomor Yang dapat dihubungi",
              type: TextInputType.phone,
            ),
            InputForm(
              controller: cityController,
              name: "Kota Asal",
              hintText: "Tulisan Nama Kota",
              type: TextInputType.text,
            ),
            InputForm(
              controller: provinceController,
              name: "Provinsi",
              hintText: "Tulisan Nama Provinsi",
              type: TextInputType.text,
            ),
            InputForm(
              controller: addressController,
              name: "Alamat Lengkap",
              hintText: "Contoh: Blok Gang, desa Desa, Kab Cirebon",
              type: TextInputType.text,
              minLine: 4,
              maxLine: 5,
            ),
            InputForm(
              controller: postalCodeController,
              name: "Kode Pos",
              hintText: "Contoh: 45192",
              type: TextInputType.number,
            ),
            ProcessButton(
              title: "Simpan Data",onClick: (){
                controller.saveFirstAddress(
                  nameAddress: nameAddressController.text,
                  receiverName: receiverNameController.text,
                  phone: phoneController.text,
                  city: cityController.text,
                  province: provinceController.text,
                  address: addressController.text,
                  kodePos: postalCodeController.text,
                );
            },
            )
          ],

        ),
      ),
    );
  }
}
