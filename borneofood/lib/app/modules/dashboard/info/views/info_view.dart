import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/info_controller.dart';

class InfoView extends GetView<InfoController> {
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Info Toko",
      body: Container(
        margin: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Image.asset(
                "assets/logo.png",
                width: 120,
                height: 120,
              ),
            ),
            SizedBox(
              height: 24,
            ),
            Text(
              "Borneofood Mart",
              style: mainText.copyWith(
                fontSize: 24.0,
              ),
            ),
            Text(
              "Perusahaan Distributor Resmi Daging Beku" +
                  " Dan produsen hasil olahan daging di Ka" +
                  "limantan Barat.",
              style: mainText.copyWith(
                color: Colors.black38,
              ),
            ),
            ProcessButton(
              title: "Cara Belanja(FAQ):",
              onClick: (){
                Get.toNamed(Routes.FAQ);
              },
            )
          ],
        ),
      ),
    );
  }
}
