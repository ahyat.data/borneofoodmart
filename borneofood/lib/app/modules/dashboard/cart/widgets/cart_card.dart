import 'package:borneofood/app/data/models/cart_model.dart';
import 'package:borneofood/app/global_widgets/circle_button.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CartCard extends StatelessWidget {
  const CartCard({
    Key key,
    @required this.controller,
    this.element,
  }) : super(key: key);

  final CartController controller;
  final Cart element;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
              vertical: 4,
              horizontal: 8.0,
            ),
            child: Row(
              children: [
                CachedNetworkImage(
                  imageUrl: element.food.photoUrl,
                  imageBuilder: (ctx, imageProv) => Container(
                    width: 90,
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        image: DecorationImage(
                            image: imageProv, fit: BoxFit.cover)),
                  ),
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  width: 8,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      element.food.name,
                      maxLines: 1,
                      style: mainText,
                    ),
                    Text(
                      formatCurrency(element.food.price),
                      style: mainText.copyWith(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(),
          Container(
            margin: EdgeInsets.only(
              left: 16,
              right: 16,
              bottom: 8,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                CircleButton(
                  icon: Icons.delete,
                  onClick: () {
                    controller.removeCart(element);
                  },
                ),
                Expanded(child: SizedBox()),
                CircleButton(
                  icon: Icons.remove,
                  bgcolor: (element.qty > 1) ? mainColor : Colors.grey,
                  iconcolor: Colors.white,
                  onClick: (element.qty > 1)
                      ? () {
                          controller.updateQty(element, -1);
                        }
                      : null,
                ),
                SizedBox(
                  width: 2,
                ),
                Container(
                  width: 50,
                  height: 25,
                  child: TextField(
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    onSubmitted: (text) {
                      final qty = int.parse(text);
                      if (qty >= 1) {
                        controller.updateQty(element, (qty - element.qty));
                      }
                    },
                    style: mainText.copyWith(
                      color: Colors.grey,
                    ),
                    decoration: InputDecoration(
                      isDense: true,
                    ),
                    controller:
                        new TextEditingController(text: element.qty.toString()),
                  ),
                ),
                SizedBox(
                  width: 2,
                ),
                CircleButton(
                  icon: Icons.add,
                  bgcolor: mainColor,
                  iconcolor: Colors.white,
                  onClick: () {
                    controller.updateQty(element, 1);
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
