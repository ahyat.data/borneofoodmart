import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/modules/dashboard/cart/widgets/cart_card.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/cart_controller.dart';

class CartView extends GetView<CartController> {
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Keranjang",
      body: Container(
        margin: EdgeInsets.all(16),
        child: Obx(
          () => Column(
            children: [
              ...controller.carts
                  .map(
                    (element) => CartCard(
                      controller: controller,
                      element: element,
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ),
    );
  }
}
