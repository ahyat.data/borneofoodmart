import 'dart:convert';

import 'package:borneofood/app/data/models/cart_model.dart';
import 'package:borneofood/app/data/models/food_model.dart';
import 'package:get/get.dart';

class CartController extends GetxController {
  final carts = <Cart>[].obs;
  int get count =>
      carts.fold(0, (previousValue, element) => previousValue + element.qty);
  double get totalPrice =>
      carts.fold(0, (previousValue, element) => previousValue + element.total);

  bool isIncart(Food food) {
    try {
      var cart = carts.firstWhere((element) => element.food.id == food.id);
      if (cart != null) {
        return true;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void addTocart(Food food) {
    try {
      var cart = carts.firstWhere((element) => element.food.id == food.id);
      final index = carts.indexOf(cart);
      cart.qty = cart.qty + 1;
      cart.total = cart.qty.toDouble() * food.price;
      carts[index] = cart;
    } catch (e) {
      carts.add(Cart(food: food, qty: 1, total: 1.0 * food.price));
    }
  }

  String total(Food food) {
    try {
      var cart = carts.firstWhere((element) => element.food.id == food.id);
      return cart.qty.toString();
    } catch (e) {
      return "0";
    }
  }

  void minQuantity(Food food) {
    try {
      var cart = carts.firstWhere((element) => element.food.id == food.id);
      final index = carts.indexOf(cart);
      if (cart.qty <= 1) {
        carts.removeAt(index);
      } else {
        cart.qty = cart.qty - 1;
        cart.total = cart.qty.toDouble() * food.price;
        carts[index] = cart;
      }
    } catch (e) {
      print("No");
    }
  }

  removeCart(Cart element) {
    carts.remove(element);
  }

  void updateQty(Cart element, int i) {
    element.qty = element.qty + i;
    final index = carts.indexOf(element);
    carts[index] = element;
  }

  int foodCount(Food food) {
    try {
      var cart = carts.firstWhere((element) => element.food.id == food.id);
      return cart.qty;
    } catch (e) {
      return 0;
    }
  }

  void pesan() {

  }
}
