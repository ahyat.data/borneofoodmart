import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../controllers/faq_controller.dart';

class FaqView extends GetView<FaqController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0),
          ),
        ),
        title: Text(
          "FAQ",
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: Obx(
        () {
          return SafeArea(
            child: Stack(
              children: [
                WebView(
                  onPageStarted: (url){
                    controller.startLoad();
                  },
                  initialUrl: controller.url,
                  javascriptMode: JavascriptMode.unrestricted,
                  onPageFinished: (url) {
                    controller.finishLoadUrl();
                  },
                ),
                controller.isLoading.value
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : Stack(),
              ],
            ),
          );
        },
      ),
    );
  }
}
