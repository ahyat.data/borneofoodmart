import 'package:get/get.dart';

class FaqController extends GetxController {
  var url="https://borneofoodstore.com/faq/18098/";
  final isLoading = true.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void finishLoadUrl() {
    isLoading(false);
  }

  void startLoad() {
    isLoading(true);
  }
}
