import 'dart:async';
import 'package:borneofood/app/data/models/banner_model.dart';
import 'package:borneofood/app/data/models/category_model.dart';
import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/data/providers/banner_provider.dart';
import 'package:borneofood/app/data/providers/category_provider.dart';
import 'package:borneofood/app/data/providers/food_provider.dart';
import 'package:borneofood/app/data/providers/transaction_provider.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/add_address_view.dart';
import 'package:borneofood/app/modules/dashboard/shoping/views/check_out_guest_view.dart';
import 'package:borneofood/app/modules/dashboard/shoping/views/check_out_view.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:whatsapp_unilink/whatsapp_unilink.dart';
import 'package:url_launcher/url_launcher.dart';

class ShopingController extends GetxController {
  FoodProvider foodProvider = Get.find();
  AuthController authController = Get.find();
  CategoryProvider categoryProvider = Get.find();
  BannerProvider bannerProvider = Get.find();
  TransactionProvider transactionProvider = Get.put(TransactionProvider());
  final CartController cartController = Get.find();
  final foods = List<Food>.empty().obs;
  final categories = List<Category>.empty().obs;
  final banners = List<BannerModel>.empty().obs;
  final address = List<BannerModel>.empty().obs;
  var currentBanner = 0.obs;

  final isAddressLoading = false.obs;

  final isFoodLoading = false.obs;
  final isCategoryLoading = false.obs;

  PageController bannerController;
  final selectedCategory = Rx<Category>(null);

  final selectedAddress = Rx<User>(null);

  @override
  void onInit() {
    super.onInit();
    getAllCategories();
    getAllFoods();
    getAllBanners();
    bannerController = PageController(initialPage: 0);
    Timer.periodic(Duration(seconds: 5), (timer) {
      if (currentBanner < banners.length - 1) {
        currentBanner++;
      } else {
        currentBanner(0);
      }
      if (bannerController.hasClients) {
        bannerController.animateToPage(
          currentBanner.value,
          duration: Duration(microseconds: 350),
          curve: Curves.easeIn,
        );
      }
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getAllFoods() async {
    isFoodLoading(true);
    final response = await foodProvider.getAll();
    if (response.body != null) {
      foods(response.body);
      isFoodLoading(false);
    } else {
      // final json = jsonDecode(response.bodyString);
      Get.snackbar("Information", response.statusText);
    }
    isFoodLoading(false);
  }

  void getAllCategories() async {
    isCategoryLoading(true);
    final data = await categoryProvider.getAll();
    if (data.body != null) {
      categories(data.body);
      isCategoryLoading(false);
    }
    isCategoryLoading(false);
  }

  Future<void> getAllBanners() async {
    final response = await bannerProvider.getAll();
    if (response.isOk) {
      final listBanner = (response.body["data"] as Iterable).map((json) {
        return BannerModel.fromJson(json);
      }).toList();
      banners(listBanner);
    }
  }

  void setBanner(int value) {
    currentBanner(value);
  }

  Future<void> getFoodByName(String query) async {
    isFoodLoading(true);
    final response = await foodProvider.getByQuery(name: query);
    isFoodLoading(false);
    if (response.body != null) {
      foods(response.body);
    }
  }

  Future<void> pesan() async {
    if (authController.authState.value is AuthenticationAuthed) {
      Get.to(() => CheckOutView());
    } else {
      Get.to(() => CheckOutGuestView());
    }
  }

  Future<void> getFoodByCategory(Category category) async {
    isFoodLoading(true);
    final response = await foodProvider.getByQuery(categoryId: category.id);
    isFoodLoading(false);
    if (response.body != null) {
      foods(response.body);
    }
    selectedCategory(category);
  }

  void selectAddress() {
    Get.bottomSheet(
      Container(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Pilih Alamat",
                  style: mainText,
                ),
                IconButton(
                    splashRadius: 24,
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
            Divider(
              height: 2,
            ),
            Obx(() => isAddressLoading.value
                ? Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Expanded(
                    child: address.length > 0
                        ? ListView(
                            children: List.generate(
                                5,
                                (index) => ListTile(
                                      title: Text("$index"),
                                    )),
                          )
                        : Center(
                            child: Text("Alamat Masih Kosong"),
                          ),
                  )),
            ProcessButton(
              title: "Tambah Alamat",
              onClick: () async {
                await Get.to(() => AddAddressView());
              },
            )
          ],
        ),
      ),
      backgroundColor: Colors.white,
      isScrollControlled: true,
    );
  }

  void openWhatsapp() async {
    final link = WhatsAppUnilink(phoneNumber: "+6281352559997", text: "");
    await launch('$link');
  }
}
