import 'package:borneofood/app/data/models/address_model.dart';
import 'package:borneofood/app/data/models/cart_model.dart';
import 'package:borneofood/app/data/models/delivery_model.dart';
import 'package:borneofood/app/data/models/transaction_model.dart';
import 'package:borneofood/app/data/providers/address_provider.dart';
import 'package:borneofood/app/data/providers/delivery_provider.dart';
import 'package:borneofood/app/data/providers/transaction_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/controllers/shoping_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/views/payment_page_view.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CheckoutController extends GetxController {
  final isDeliveryLoading = false.obs;
  final isAddressLoading = false.obs;
  CartController cartController = Get.find();
  final DeliveryProvider deliveryProvider = Get.put(DeliveryProvider());
  final TransactionProvider transactionProvider =
      Get.put(TransactionProvider());
  final AddressProvider addressProvider =
  Get.put(AddressProvider());
  var selectedDeliveryMethod = Rx<DeliveryModel>(null);
  var selectedAddress = Rx<AddressModel>(null);
  var deliveries = RxList<DeliveryModel>();
  var addresses = RxList<AddressModel>();

  TextEditingController nameController,
      phoneController,
      emailController,
      cityController,
      addressController,
      postalCodeController,
      provinceController;



  @override
  void onInit() {
    nameController = TextEditingController();
    phoneController = TextEditingController();
    emailController = TextEditingController();
    cityController = TextEditingController();
    addressController = TextEditingController();
    postalCodeController = TextEditingController();
    provinceController = TextEditingController();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void selectAddress() async{
    getAddresses();
    final address = await Get.bottomSheet(
      Column(
        children: [
          Padding(
            padding:
            const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Pilih Alamat",
                  style: mainText,
                ),
                IconButton(
                    splashRadius: 24,
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ),
          Divider(
            height: 2,
          ),
          Obx(() => isAddressLoading.value
              ? Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
              : Expanded(
            child: ListView.builder(
              itemBuilder: (ctx, index) => ListTile(
                title: Text(
                  "${addresses[index].name} (${addresses[index].phone})",
                  style: mainText.copyWith(
                    fontSize: 14.0,
                  ),
                ),
                subtitle: Text("${addresses[index].address}"),
                onTap: () {
                  Get.back(result: addresses[index]);
                },
              ),
              itemCount: addresses.length,
            ),
          ))
        ],
      ),
      backgroundColor: Colors.white,
      isDismissible: false,
    );
    if(address != null){
      selectedAddress(address);
    }
  }

  Future<void> selectDeliveryMethod() async {
    getDeliveries();
    final delivery = await Get.bottomSheet(
      Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Pilih Metode Pengiriman",
                  style: mainText,
                ),
                IconButton(
                    splashRadius: 24,
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ),
          Divider(
            height: 2,
          ),
          Obx(() => isDeliveryLoading.value
              ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : Expanded(
                  child: ListView.builder(
                    itemBuilder: (ctx, index) => ListTile(
                      title: Text(
                        "${deliveries[index].name} (${formatCurrency(deliveries[index].price)})",
                        style: mainText.copyWith(
                          fontSize: 14.0,
                        ),
                      ),
                      subtitle: Text("${deliveries[index].description}"),
                      onTap: () {
                        Get.back(result: deliveries[index]);
                      },
                    ),
                    itemCount: deliveries.length,
                  ),
                ))
        ],
      ),
      backgroundColor: Colors.white,
      isDismissible: false,
    );
    if (delivery != null) {
      selectedDeliveryMethod(delivery);
    }
  }

  Future<void> getDeliveries() async {
    isDeliveryLoading(true);
    final response = await deliveryProvider.getAll();
    isDeliveryLoading(false);
    if (response.isOk) {
      final data = (response.body["data"] as Iterable)
          .map((e) => DeliveryModel.fromJson(e))
          .toList();
      deliveries(data);
    }
  }

  createGuestTransaction(List<Cart> carts) async {
    if (selectedDeliveryMethod.value == null) {
      showErrorMessage("Silahkan Pilih Metode Pembayaran terlebih dahulu");
    } else {
      showLoadingWidget();
      final response = await transactionProvider.createGuestTransaction(
        carts: carts,
        deliveryId: selectedDeliveryMethod.value.id,
        name: nameController.text,
        email: emailController.text,
        city: cityController.text,
        postalCode: postalCodeController.text,
        address: addressController.text,
        phone: phoneController.text,
        province: provinceController.text,
        totalPrice: carts.fold(
            0, (previousValue, element) => previousValue + element.total),
      );
      closeLoadingWidget();
      if (response.isOk) {
        cartController.carts.value = <Cart>[];
        final transaction = Transaction.fromJson(response.body["data"]);
        await Get.to(() => PaymentPageView(url: transaction.paymentUrl,));
        final ShopingController shopingController = Get.find();
        shopingController.getAllFoods();
        Get.back();
      } else {
        showErrorMessage(response.body["meta"]["message"]);
      }
    }
  }

  Future<void> getAddresses() async {
    isAddressLoading(true);
    final response = await addressProvider.getAll();
    isAddressLoading(false);
    if(response.isOk){
      final data = (response.body["data"] as Iterable).map((e) => AddressModel.fromJson(e)).toList();
      addresses(data);
    }
  }

  Future<void> createTransaction({List<Cart> carts}) async {
    if (selectedDeliveryMethod.value == null || selectedAddress.value == null) {
      showErrorMessage("Silahkan Pilih Alamat dan Metode Pembayaran terlebih dahulu");
    }else{
      showLoadingWidget();
      final response = await transactionProvider.createTransaction(
        totalPrice: carts.fold(
            0, (previousValue, element) => previousValue + element.total),
        addressId: selectedAddress.value.id,
        deliveryId: selectedDeliveryMethod.value.id,
        carts: carts
      );
      closeLoadingWidget();
      print(response.body);
      if (response.isOk) {
        cartController.carts.value = <Cart>[];
        final transaction = Transaction.fromJson(response.body["data"]);
        await Get.to(() => PaymentPageView(url: transaction.paymentUrl,));
        final ShopingController shopingController = Get.find();
        shopingController.getAllFoods();
        Get.back();
      } else {
        showErrorMessage(response.body["meta"]["message"]);
      }
    }
  }
}
