import 'package:get/get.dart';

import 'package:borneofood/app/modules/dashboard/shoping/controllers/checkout_controller.dart';

import '../controllers/shoping_controller.dart';

class ShopingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ShopingController>(
      () => ShopingController(),
    );
  }
}
