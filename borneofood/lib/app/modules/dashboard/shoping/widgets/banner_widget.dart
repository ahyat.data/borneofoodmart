import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:borneofood/app/data/models/banner_model.dart';

class BannerWidget extends StatelessWidget {
  final BannerModel banner;

  const BannerWidget({Key key, this.banner}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FittedBox(
        fit: BoxFit.cover,
        child: CachedNetworkImage(
          imageUrl: banner.imageUrl,
          progressIndicatorBuilder: (ctx, str, a) => Center(
            child: CircularProgressIndicator(),
          ),
        ),
      ),
    );
  }
}
