import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/global_widgets/circle_button.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/tag_promo.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/tag_tersedia.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FoodCard extends StatelessWidget {
  final controller = Get.put(CartController());
  FoodCard({
    Key key,
    @required this.food,
  }) : super(key: key);

  final Food food;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(food.photoUrl),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      topRight: Radius.circular(8.0),
                      bottomRight: Radius.circular(16.0),
                    ),
                  ),
                ),
                Positioned(
                  child: TagTersedia(
                    stock: food.qty,
                  ),
                  bottom: 0,
                  left: 0,
                ),
                food.promo
                    ? Positioned(
                        child: TagPromo(),
                        right: 0,
                        top: 0,
                      )
                    : SizedBox(),
              ],
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    food.name,
                    style: mainText.copyWith(
                      fontSize: 14.0,
                    ),
                  ),
                  Text(
                    "${formatCurrency(food.price)}",
                    style: mainText.copyWith(
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Obx(
                    () => (!controller.isIncart(food))
                        ? Container(
                            width: double.infinity,
                            child: ElevatedButton.icon(
                              style:
                                  ElevatedButton.styleFrom(primary: mainColor),
                              onPressed: food.qty > 0 ? () {
                                controller.addTocart(food);
                              }: null,
                              icon: Icon(Icons.shopping_bag_outlined),
                              label: Text("Pesan"),
                            ),
                          )
                        : Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(
                                horizontal: 4.0, vertical: 4.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8.0),
                              border: Border.all(color: Colors.deepOrange),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                CircleButton(
                                  icon: Icons.add,
                                  bgcolor:
                                      (controller.foodCount(food) < food.qty)
                                          ? mainColor
                                          : Colors.grey,
                                  iconcolor: Colors.white,
                                  onClick:
                                      (controller.foodCount(food) < food.qty)
                                          ? () {
                                              controller.addTocart(food);
                                            }
                                          : null,
                                ),
                                Text(
                                  controller.total(food),
                                  textAlign: TextAlign.center,
                                ),
                                CircleButton(
                                  icon: Icons.remove,
                                  bgcolor: mainColor,
                                  iconcolor: Colors.white,
                                  onClick: () {
                                    controller.minQuantity(food);
                                  },
                                ),
                              ],
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
