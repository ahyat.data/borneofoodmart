import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class TagTersedia extends StatelessWidget {
  final int stock;
  const TagTersedia({Key key, this.stock}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 1,
        horizontal: 3,
      ),
      decoration: BoxDecoration(
        color: (this.stock <= 0) ? Colors.white : Colors.black,
        // border: Border.all(),
      ),
      child: Text(
        (this.stock <= 0) ? 'Habis' : 'Tersedia',
        style: (this.stock <= 0)
            ? mainText.copyWith(
                color: mainColor,
              )
            : mainText.copyWith(
                color: Colors.white,
              ),
      ),
    );
  }
}
