import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class TagPromo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 1,
        horizontal: 3,
      ),
      decoration: BoxDecoration(
          color: Colors.orange,
          borderRadius: BorderRadius.only(topRight: Radius.circular(8))
          // border: Border.all(),
          ),
      child: Text(
        'Promo',
        style: mainText.copyWith(
          color: Colors.white,
        ),
      ),
    );
  }
}
