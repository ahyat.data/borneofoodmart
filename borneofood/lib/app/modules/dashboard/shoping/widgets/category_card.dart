import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  final String name;
  final bool isSelected;
  final Function onTap;

  CategoryCard(this.name, {this.isSelected = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (this.onTap != null) {
          this.onTap();
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        height: 30,
        margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 8.0),
        decoration: BoxDecoration(
          color: this.isSelected ? Colors.orange : Colors.deepOrangeAccent,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.transparent,
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child: Center(
            child: Text(
          this.name,
          style: TextStyle(color: Colors.white, fontSize: 15),
        )),
      ),
    );
  }
}
