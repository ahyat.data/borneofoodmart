import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/controllers/checkout_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/controllers/shoping_controller.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class CheckOutView extends StatelessWidget {
  final CartController cartController = Get.find();
  final CheckoutController controller = Get.put(CheckoutController());
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Checkout Pesanan",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "List Pesanan",
              style: mainText.copyWith(
                fontSize: 16.0,
              ),
            ),
            ...cartController.carts
                .map((element) => ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: Image.network(
                element.food.photoUrl,
                width: 60,
                height: 40,
                fit: BoxFit.cover,
              ),
              title: Text(
                element.food.name,
                style: mainText.copyWith(
                  fontSize: 14,
                ),
              ),
              subtitle: Text("${element.qty.toString()} buah"),
            ))
                .toList(),
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Total Pesanan", style: mainText,),
                      Text("${formatCurrency(cartController.carts.fold(0, (previousValue, element) => previousValue + element.total))}", style: mainText,),
                    ],
                  ),
                ),
                ActionButton(
                  text: "Tambah Pesanan",
                  onClick: (){
                    Get.back();
                  },
                )
              ],
            ),
            Divider(),
            Text("Alamat Pengiriman", style: mainText,),
            SizedBox(height: 8.0,),
            GestureDetector(
              onTap: (){
                controller.selectAddress();
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    )),
                width: double.infinity,
                child: Obx(() {
                  return Row(
                    children: [
                      Expanded(
                        child: Text(controller.selectedAddress.value != null
                            ? controller.selectedAddress.value.name
                            : "Pilih Alamat Pengiriman..", style: mainText,),
                      ),
                      Icon(Icons.arrow_drop_down),
                    ],
                  );
                }),
              ),
            ),
            SizedBox(height: 8.0,),
            Text("Metode Pengiriman", style: mainText,),
            SizedBox(height: 8.0,),
            GestureDetector(
              onTap: (){
                controller.selectDeliveryMethod();
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    )),
                width: double.infinity,
                child: Obx(() {
                  return Row(
                    children: [
                      Expanded(
                        child: Text(controller.selectedDeliveryMethod.value != null
                            ? "${controller.selectedDeliveryMethod.value.name} (${formatCurrency(controller.selectedDeliveryMethod.value.price)})"
                            : "Pilih Metode Pengiriman..", style: mainText,),
                      ),
                      Icon(Icons.arrow_drop_down),
                    ],
                  );
                }),
              ),
            ),
          ],
        ),
      ),
      bottomWidget: ElevatedButton(
        child: Text("Bayar"),
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.all(22.0),
          primary: mainColor,
          onPrimary: Colors.white,
          textStyle: mainText.copyWith(
            fontSize: 14.0,
          ),
          shape: BeveledRectangleBorder(), // foreground
        ),
        onPressed: (){
          controller.createTransaction(carts: cartController.carts);
        },
      )
    );
  }
}
