import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/global_widgets/search_field_widget.dart';
import 'package:borneofood/app/global_widgets/shimer_grid_loading.dart';
import 'package:borneofood/app/modules/dashboard/cart/controllers/cart_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/banner_indicator.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/banner_widget.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/category_card.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/food_card.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'package:get/get.dart';

import '../controllers/shoping_controller.dart';

class ShopingView extends GetWidget<ShopingController> {
  final searchController = TextEditingController();
  final CartController cartController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: mainColor,
          ),
          SafeArea(
            child: Container(
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    height: 180,
                    width: double.infinity,
                    child: Stack(
                      children: [
                        Obx(() => Container(
                              child: PageView.builder(
                                controller: controller.bannerController,
                                onPageChanged: (value) =>
                                    controller.setBanner(value),
                                itemCount: controller.banners.length,
                                itemBuilder: (ctx, index) => BannerWidget(
                                  banner: controller.banners[index],
                                ),
                              ),
                            )),
                        Container(
                          margin: EdgeInsets.only(
                              top: 16.0, left: 16.0, right: 16.0),
                          child: Row(
                            children: [
                              Expanded(
                                  child: SearchFieldWidget(
                                controller: searchController,
                                placeHolder: "Cari Produk",
                                onSearch: (query) {
                                  controller.getFoodByName(query);
                                },
                              )),
                              SizedBox(
                                width: 16,
                              ),
                              Container(
                                width: 45,
                                height: 45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: mainColor,
                                ),
                                child: IconButton(
                                    icon: Icon(
                                      Icons.chat_bubble,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      controller.openWhatsapp();
                                    }),
                              )
                            ],
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          child: Container(
                            padding: EdgeInsets.all(3),
                            child: Text(
                              'Borneofood',
                              style: mainText.copyWith(
                                color: Colors.white,
                              ),
                            ),
                            decoration: BoxDecoration(
                              color: Colors.deepOrange,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Obx(() => Container(
                        margin: EdgeInsets.only(bottom: 4.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: List.generate(
                                controller.banners.length,
                                (index) => BannerIndicator(
                                      index: index,
                                      currentBanner:
                                          controller.currentBanner.value,
                                    ))),
                      )),
                  Container(
                    height: 60,
                    color: Colors.white,
                    child: Obx(
                      () => ListView(
                        scrollDirection: Axis.horizontal,
                        children: controller.categories
                            .map(
                              (e) => CategoryCard(
                                e.name,
                                isSelected:
                                    e == controller.selectedCategory.value,
                                onTap: () {
                                  controller.getFoodByCategory(e);
                                },
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.all(8.0),
                        child: RefreshIndicator(
                          onRefresh: () async {
                            controller.getAllFoods();
                            searchController.text = "";
                          },
                          child: Obx(
                            () => (controller.isFoodLoading.value)
                                ? ShimmerGridLoading()
                                : StaggeredGridView.countBuilder(
                                    crossAxisCount: 2,
                                    itemCount: controller.foods.length,
                                    crossAxisSpacing: 4,
                                    mainAxisSpacing: 4,
                                    itemBuilder: (context, index) {
                                      Food food = controller.foods[index];
                                      return FoodCard(food: food);
                                    },
                                    staggeredTileBuilder: (index) =>
                                        StaggeredTile.fit(1),
                                  ),
                          ),
                        )),
                  ),
                  Obx(
                    () => (cartController.carts.isNotEmpty)
                        ? Container(
                            color: mainColor[600],
                            height: 45,
                            width: double.infinity,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  height: double.infinity,
                                  margin: EdgeInsets.all(4.0),
                                  padding: EdgeInsets.all(4.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(
                                      color: Colors.white,
                                    ),
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        height: 30,
                                        width: 30,
                                        margin: EdgeInsets.only(right: 16.0),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                        ),
                                        child: Text(
                                          cartController.count.toString(),
                                        ),
                                      ),
                                      RichText(
                                        text: TextSpan(
                                          text: "TOTAL  ",
                                          style: mainText,
                                          children: [
                                            TextSpan(
                                              text: formatCurrency(
                                                  cartController.totalPrice),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    print("Post Data Pesan");
                                    controller.pesan();
                                  },
                                  child: Container(
                                    height: double.infinity,
                                    margin: EdgeInsets.all(4.0),
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 16.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(
                                        color: Colors.white,
                                      ),
                                    ),
                                    child: Center(
                                        child: Text(
                                      "Pesan",
                                      style: mainText.copyWith(
                                        color: Colors.white,
                                      ),
                                    )),
                                  ),
                                )
                              ],
                            ),
                          )
                        : SizedBox(),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
