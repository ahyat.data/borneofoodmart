import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class PaymentPageView extends StatefulWidget {
  final String url;

  const PaymentPageView({Key key, this.url}) : super(key: key);
  @override
  _PaymentPageViewState createState() => _PaymentPageViewState();
}

class _PaymentPageViewState extends State<PaymentPageView> {
  bool loading = true;
  WebViewController webController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () async {
            final url = await webController.currentUrl();
            if(url == 'https://borneo.zeegoodss.com/midtrans/success'){
              Get.back();
            }else{
              if(await webController.canGoBack()){
                webController.goBack();
              }
            }
          }, icon: Icon(Icons.arrow_back_ios_outlined),
        ),
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0),
          ),
        ),
        title: Text(
          "Metode Pembayaran",
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            WebView(
              initialUrl: widget.url,
              navigationDelegate: (request){
                if(!request.url.startsWith('http')){
                  launch(request.url);
                  return NavigationDecision.prevent;
                }
                return NavigationDecision.navigate;
              },
              onPageFinished: (url){
                setState(() {
                  loading = false;
                });
              },
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (_controller){
                setState(() {
                  webController = _controller;
                });
              },
            ),
            loading
                ? Center(
              child: CircularProgressIndicator(),
            )
                : Stack(),
          ],
        ),
      ),
    );
  }
}
