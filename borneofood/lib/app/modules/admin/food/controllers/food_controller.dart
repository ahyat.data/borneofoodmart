import 'dart:io';

import 'package:borneofood/app/data/models/category_model.dart';
import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/data/providers/category_provider.dart';
import 'package:borneofood/app/data/providers/food_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/admin/food/views/add_food_view.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class FoodController extends GetxController {
  FoodProvider foodProvider = Get.find();
  CategoryProvider categoryProvider = Get.find();
  final foods = List<Food>.empty().obs;
  final isFoodLoading = false.obs;
  final isCategoryLoading = false.obs;
  final isPromo = false.obs;
  final filterPromo = false.obs;
  final filterStock = false.obs;
  File foodImage;
  final categories = RxList<Category>();
  final selectedCategory = Rx<Category>(null);

  @override
  void onInit() {
    super.onInit();
    getFoods();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getFoods() async {
    isFoodLoading(true);
    try {
      final response = await foodProvider.getAll(
          promo: filterPromo.value, stock: filterStock.value);
      print(response.body);
      if (response.body != null) {
        foods(response.body);
      }
    } catch (err) {
      print(err);
    }

    isFoodLoading(false);
  }

  Future<void> createFood(
    String name,
    String price,
    String qty,
    String desc,
  ) async {
    showLoadingWidget();
    final response = await foodProvider.createFood(
        name: name,
        price: price,
        desc: desc,
        qty: qty,
        isPromo: isPromo.value,
        categoryId: selectedCategory.value.id,
        file: foodImage);
    closeLoadingWidget();
    print(response.body);
    if (response.isOk) {
      getFoods();
      Get.back();
      foodImage = null;
      showSuccessMessage(response.body["meta"]["message"]);
    } else {
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  void deleteFood(Food food) async {
    showLoadingWidget();
    final response = await foodProvider.deleteFood(food);
    closeLoadingWidget();
    if (response.isOk) {
      getFoods();
      showSuccessMessage(response.body["meta"]["message"]);
    } else {
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  void editFood(Food food) {
    Get.to(() => AddFoodView(food: food));
  }

  void updateFood(
    String name,
    String price,
    String qty,
    String desc,
    Food food,
  ) async {
    showLoadingWidget();
    final response = await foodProvider.updateFood(
      name: name,
      price: price,
      desc: desc,
      qty: qty,
      isPromo: isPromo.value,
      food: food,
      file: foodImage,
      categoryId: selectedCategory.value.id,
    );
    closeLoadingWidget();
    print(response.body);
    if (response.isOk) {
      foodImage = null;
      Get.back();
      getFoods();
      showSuccessMessage(response.body["meta"]["message"]);
    } else {
      print(response.body);
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  void getByName(String name) async {
    isFoodLoading(true);
    final response = await foodProvider.getByQuery(
        name: name, promo: filterPromo.value, stock: filterStock.value);
    if (response.body != null) {
      foods(response.body);
    }
    isFoodLoading(false);
  }

  Future<void> selectFoodPhoto() async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      foodImage = File(pickedFile.path);
      update();
    }
  }

  Future<void> getCategories() async {
    isCategoryLoading(true);
    final response = await categoryProvider.getAll();
    if (response.body != null) {
      categories(response.body);
    }
    isCategoryLoading(false);
  }

  void setCategory(value) {
    selectedCategory.value = value;
  }

  Future<void> selectCategory() async {
    getCategories();
    final category = await Get.bottomSheet(
      Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Pilih Kategori",
                  style: mainText,
                ),
                IconButton(
                    splashRadius: 24,
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ),
          Divider(
            height: 2,
          ),
          Obx(() => isCategoryLoading.value
              ? Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                )
              : Expanded(
                  child: ListView.builder(
                    itemBuilder: (ctx, index) => ListTile(
                      title: Text(
                        categories[index].name,
                        style: mainText.copyWith(
                          fontSize: 14.0,
                        ),
                      ),
                      onTap: () {
                        Get.back(result: categories[index]);
                      },
                    ),
                    itemCount: categories.length,
                  ),
                ))
        ],
      ),
      backgroundColor: Colors.white,
      isDismissible: false,
    );
    if (category != null) {
      selectedCategory(category);
    }
  }

  void setFilter({bool promo, bool zero}) {
    if (promo != null) {
      filterPromo(promo);
    }
    if (zero != null) {
      filterStock(zero);
    }
    getFoods();
  }
}
