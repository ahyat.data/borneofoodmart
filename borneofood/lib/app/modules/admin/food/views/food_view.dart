import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/global_widgets/search_field_widget.dart';
import 'package:borneofood/app/global_widgets/shimmer_list_loader.dart';
import 'package:borneofood/app/modules/admin/food/widgets/food_card.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/food_controller.dart';

class FoodView extends GetWidget<FoodController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(16.0),
            bottomRight: Radius.circular(16.0),
          ),
        ),
        title: Text(
          "Produk Manajemen",
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
            child: Container(
              margin: EdgeInsets.all(12.0),
              child: SearchFieldWidget(
                onSearch: (text) {
                  controller.getByName(text);
                },
                placeHolder: "Cari Produk Yang akan di edit ?",
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
            child: Row(
              children: [
                Obx(() => FilterChip(
                      selected: controller.filterPromo.value,
                      label: Text(
                        "Promo",
                        style: mainText.copyWith(
                          color: Colors.white,
                        ),
                      ),
                      onSelected: (value) {
                        controller.setFilter(promo: value);
                      },
                      backgroundColor: mainColor,
                      selectedColor: Colors.pink,
                      elevation: 2,
                    )),
                SizedBox(
                  width: 8.0,
                ),
                Obx(() => FilterChip(
                      label: Text(
                        "Stock Kosong",
                        style: mainText.copyWith(
                          color: Colors.white,
                        ),
                      ),
                      onSelected: (value) {
                        controller.setFilter(zero: value);
                      },
                      selected: controller.filterStock.value,
                      backgroundColor: mainColor,
                      selectedColor: Colors.pink,
                      elevation: 2,
                    )),
              ],
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: RefreshIndicator(
                onRefresh: () async {
                  controller.getFoods();
                },
                child: Obx(
                  () => (controller.isFoodLoading.value)
                      ? ListView(
                          children: [
                            ShimmerListLoading(),
                          ],
                        )
                      : ListView.builder(
                          itemCount: controller.foods.length,
                          itemBuilder: (ctx, index) {
                            Food food = controller.foods[index];
                            return FoodCard(
                              food: food,
                              controller: controller,
                            );
                          },
                        ),
                ),
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Get.toNamed(Routes.FOOD_ADD);
        },
        backgroundColor: mainColor,
      ),
    );
  }
}
