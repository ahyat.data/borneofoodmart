import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/admin/food/controllers/food_controller.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddFoodView extends GetWidget<FoodController> {
  final Food food;
  final nameController = TextEditingController();
  final priceController = MoneyMaskedTextController(
      leftSymbol: "Rp. ",
      thousandSeparator: ",",
      decimalSeparator: "",
      precision: 0,
      initialValue: 1000);
  final qtyController = TextEditingController(text: "1");
  final descController = TextEditingController();

  AddFoodView({Key key, this.food}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (food != null) {
      nameController.text = food.name;
      priceController.text = food.price.toString();
      qtyController.text = food.qty.toString();
      descController.text = food.description;
      controller.isPromo(food.promo);
    } else {
      controller.foodImage = null;
      controller.isPromo(false);
    }
    return GeneralView(
      title: food != null ?"Edit Produk" :"Tambah Produk",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Photo Produk", style: mainText,),
            SizedBox(height: 8,),
            GestureDetector(
              onTap: controller.selectFoodPhoto,
              child: DottedBorder(
                strokeWidth: 1,
                color: Colors.grey,
                padding: EdgeInsets.all(6),
                dashPattern: [6, 6],
                child: GetBuilder(
                  init: controller,
                  builder: (value) {
                    return Container(
                      height: 120,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Color(0xFFF0F0F0),
                        image: (controller.foodImage == null)
                            ? null
                            : DecorationImage(
                                image: FileImage(controller.foodImage),
                                fit: BoxFit.cover),
                      ),
                      child: (controller.foodImage == null)
                          ? (food == null ) ?Center(
                              child: Text(
                                "Pilih Foto Produk",
                                style: mainText.copyWith(
                                  color: Color(0xFF8D92A3),
                                  fontSize: 12.0,
                                ),
                              ),
                            ): Image.network(food.photoUrl, fit: BoxFit.cover,)
                          : SizedBox(),
                    );
                  },
                ),
              ),
            ),
            InputForm(
              name: "Nama Makanan",
              hintText: "Nama Makanan",
              controller: nameController,
            ),
            GestureDetector(
              onTap: (){
                controller.selectCategory();
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    )),
                width: double.infinity,
                child: Obx(() {
                  return Row(
                    children: [
                      Expanded(
                        child: Text(controller.selectedCategory.value != null
                            ? controller.selectedCategory.value.name
                            : "Pilih Kategori..", style: mainText,),
                      ),
                      Icon(Icons.arrow_drop_down),
                    ],
                  );
                }),
              ),
            ),
            InputForm(
              name: "Harga Makanan",
              hintText: "Harga Makanan",
              type: TextInputType.number,
              controller: priceController,
            ),
            InputForm(
              name: "Jumlah Makanan",
              hintText: "Jumlah Makanan",
              type: TextInputType.number,
              controller: qtyController,
            ),
            InputForm(
              name: "Deskripsi Makanan",
              hintText: "Deskripsi Makanan",
              type: TextInputType.text,
              minLine: 3,
              maxLine: 4,
              controller: descController,
            ),
            Row(
              children: [
                Expanded(
                    child: Text(
                  "Promo",
                  style: mainText.copyWith(fontSize: 14),
                )),
                Obx(
                  () => Switch(
                    value: controller.isPromo.value,
                    onChanged: (value) {
                      controller.isPromo(value);
                    },
                  ),
                )
              ],
            )
          ],
        ),
      ),
      bottomWidget: ElevatedButton(
        onPressed: () {
          if(controller.selectedCategory.value != null){
            if (food != null) {
              controller.updateFood(
                  nameController.text,
                  priceController.numberValue.toString(),
                  qtyController.text,
                  descController.text,
                  food);
            } else {
              controller.createFood(
                  nameController.text,
                  priceController.numberValue.toString(),
                  qtyController.text,
                  descController.text);
            }
          }else{
            showErrorMessage("Silahkan pilih kategori terlebih dahulu!.");
          }
        },
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.all(20.0),
          primary: mainColor,
          onPrimary: Colors.white,
          textStyle: mainText.copyWith(
            fontSize: 14.0,
          ),
          shape: BeveledRectangleBorder(), // foreground
        ),
        child: Text("Simpan Makanan"),
      ),
    );
  }
}
