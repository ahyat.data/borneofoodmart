import 'package:borneofood/app/data/models/food_model.dart';
import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/modules/admin/food/controllers/food_controller.dart';
import 'package:borneofood/app/modules/dashboard/shoping/widgets/tag_promo.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class FoodCard extends StatelessWidget {
  const FoodCard({
    Key key,
    @required this.food,
    @required this.controller,
  }) : super(key: key);

  final Food food;
  final FoodController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Card(
          child: Column(
            children: [
              ListTile(
                leading: Container(
                  height: 40,
                  width: 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    image: DecorationImage(
                      onError: (obj, st) {
                        return Icon(Icons.error);
                      },
                      image: NetworkImage(
                        food.photoUrl,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                title: Text(
                  food.name,
                  style: mainText.copyWith(
                    fontSize: 13.0,
                  ),
                ),
                subtitle: Text(
                  "${food.qty} Pcs @ ${formatCurrency(food.price)}",
                  style: mainText.copyWith(
                    fontSize: 12.0,
                  ),
                ),
              ),
              Container(
                child: Row(
                  children: [
                    ActionButton(
                      onClick: () {
                        controller.editFood(food);
                      },
                      bgColor: Colors.amber,
                      text: "Edit",
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    ActionButton(
                      onClick: () {
                        print("delete");
                        controller.deleteFood(food);
                      },
                      bgColor: Colors.red,
                      text: "Hapus",
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                ),
                margin: EdgeInsets.all(8.0),
              )
            ],
          ),
        ),
        food.promo
            ? Positioned(
                child: TagPromo(),
                right: 4,
                top: 4,
              )
            : SizedBox(),
      ],
    );
  }
}
