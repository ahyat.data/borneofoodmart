import 'dart:io';

import 'package:borneofood/app/data/models/banner_model.dart';
import 'package:borneofood/app/data/providers/banner_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/admin/banner/views/add_edit_banner_view.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class BannerController extends GetxController {
  final banners = RxList<BannerModel>();
  final BannerProvider bannerProvider = Get.put(BannerProvider());

  File bannerImage;
  @override
  void onInit() {
    super.onInit();
    loadBanners();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> loadBanners() async {
    final response = await bannerProvider.getAll();
    if(response.isOk){
      final banner = (response.body["data"] as Iterable).map((e) => BannerModel.fromJson(e)).toList();
      banners(banner);
    }
  }
  void editBanner({BannerModel banner}) async{
    Get.to(() => AddEditBannerView(banner: banner,));
  }
  Future<void> deleteBanner({BannerModel banner}) async {
    final response = await bannerProvider.deleteBanner(bannerId: banner.id);
    if(response.isOk){
      showSuccessMessage("Data Berhasil di hapus");
      loadBanners();
    }else{
      showErrorMessage("Terjadi Masalah ketika menghapus data");
    }
  }

  void selectBannerPhoto()async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      bannerImage = File(pickedFile.path);
      update();
    }
  }

  void updateBanner({String name, BannerModel banner}) async{
    final response = await bannerProvider.editBanner(
      name: name,
      photo: bannerImage,
      banner: banner,
    );
    if(response.isOk){
      showSuccessMessage("Banner Berhasil di update");
      loadBanners();
    }else{
      print(response.body);
      showErrorMessage("Terjadi Masalah ketika memperbarui data");
    }

  }

  void createBanner({String name}) async {
    final response = await bannerProvider.createBanner(
        name: name,
        photo: bannerImage
    );
    if(response.isOk){
      showSuccessMessage("Banner Berhasil di tambahkan");
      loadBanners();
    }else{
      showErrorMessage("Terjadi Masalah ketika memperbarui data");
    }
  }

  void addBanner() {
    Get.to(() => AddEditBannerView());
  }
}
