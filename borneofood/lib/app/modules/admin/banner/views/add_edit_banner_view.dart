import 'package:borneofood/app/data/models/banner_model.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/modules/admin/banner/controllers/banner_controller.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class AddEditBannerView extends GetWidget<BannerController> {
  final BannerModel banner;
  final nameController = TextEditingController();
  AddEditBannerView({this.banner});
  @override
  Widget build(BuildContext context) {
    if(banner != null){
      nameController.text = banner.name;
    }
    return GeneralView(
      title: banner!= null ? "Edit Banner" : "Tambah Banner",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Photo Banner", style: mainText,),
            SizedBox(height: 8,),
            GestureDetector(
              onTap: controller.selectBannerPhoto,
              child: DottedBorder(
                strokeWidth: 1,
                color: Colors.grey,
                padding: EdgeInsets.all(6),
                dashPattern: [6, 6],
                child: GetBuilder(
                  init: controller,
                  builder: (value) {
                    return Container(
                      height: 120,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Color(0xFFF0F0F0),
                        image: (controller.bannerImage == null)
                            ? null
                            : DecorationImage(
                            image: FileImage(controller.bannerImage),
                            fit: BoxFit.cover),
                      ),
                      child: (controller.bannerImage == null)
                          ? (banner == null ) ?Center(
                        child: Text(
                          "Pilih Foto Banner",
                          style: mainText.copyWith(
                            color: Color(0xFF8D92A3),
                            fontSize: 12.0,
                          ),
                        ),
                      ): Image.network(banner.imageUrl, fit: BoxFit.cover,)
                          : SizedBox(),
                    );
                  },
                ),
              ),
            ),
            InputForm(
              name: "Nama Banner",
              hintText: "Nama Banner",
              controller: nameController,
            ),
          ],
        ),
      ),
      bottomWidget: ElevatedButton(
        onPressed: () {
          if(banner != null){
            controller.updateBanner(
              name: nameController.text,
              banner: banner
            );
          }else{
            controller.createBanner(
                name: nameController.text
            );
          }
        },
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.all(20.0),
          primary: mainColor,
          onPrimary: Colors.white,
          textStyle: mainText.copyWith(
            fontSize: 14.0,
          ),
          shape: BeveledRectangleBorder(), // foreground
        ),
        child: Text("Simpan Banner"),
      ),
    );
  }
}
