import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/banner_controller.dart';

class BannerView extends GetView<BannerController> {
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Banner Manajemen",
      body: Container(
        margin: EdgeInsets.all(16),
        child: Obx(
          () {
            return Column(
              children: [
                ...controller.banners.map((element) {
                  return Card(
                    child: Column(
                      children: [
                        Container(
                          height: 120,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(element.imageUrl),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Text(
                          element.name,
                          style: mainText,
                        ),
                        Container(
                          margin: EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              ActionButton(
                                text: "Edit",
                                onClick: () {
                                  controller.editBanner(banner: element);
                                },
                              ),
                              SizedBox(
                                width: 16.0,
                              ),
                              ActionButton(
                                text: "Hapus",
                                onClick: () {
                                  controller.deleteBanner(banner: element);
                                },
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                }).toList()
              ],
            );
          },
        ),
      ),
      floatingWidget: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          controller.addBanner();
        },
      ),
    );
  }
}
