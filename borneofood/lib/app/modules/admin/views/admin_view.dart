import 'package:borneofood/app/modules/admin/category/views/category_view.dart';
import 'package:borneofood/app/modules/admin/food/views/food_view.dart';
import 'package:borneofood/app/modules/admin/setting/views/setting_view.dart';
import 'package:borneofood/app/modules/admin/transaction/views/transaction_view.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/admin_controller.dart';

class AdminView extends GetView<AdminController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: controller.pageController,
        children: [
          FoodView(),
          CategoryView(),
          TransactionView(),
          SettingView(),
        ],
      ),
      bottomNavigationBar: Obx(() => BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: mainColor,
            selectedItemColor: Colors.white,
            selectedLabelStyle: mainText,
            unselectedLabelStyle: mainText,
            unselectedItemColor: Colors.white54,
            elevation: 0,
            currentIndex: controller.pageIndex.value,
            onTap: (index) {
              controller.serIndexPage(index);
            },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.food_bank),
                label: "Produk",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.category_rounded),
                label: "Kategori",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.shopping_bag_rounded),
                label: "Transaksi",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings_outlined),
                label: "Pengaturan",
              ),
            ],
          )),
    );
  }
}
