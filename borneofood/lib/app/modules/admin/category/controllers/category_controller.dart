import 'dart:io';

import 'package:borneofood/app/data/models/category_model.dart';
import 'package:borneofood/app/data/providers/category_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/admin/category/widgets/botoom_sheet.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class CategoryController extends GetxController {
  File categoryFile;
  CategoryProvider categoryProvider = Get.find();
  final isCategoryLoading = false.obs;
  TextEditingController nameController;
  final categories = List<Category>.empty().obs;

  @override
  void onInit() {
    super.onInit();
    nameController = TextEditingController();
    getCategories();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getCategories() async {
    isCategoryLoading(true);
    final response = await categoryProvider.getAll();
    isCategoryLoading(false);
    if (response.body != null) {
      categories(response.body);
    }
  }

  void addCategory() {
    nameController.clear();
    addOrEditCategory(nameController, this);
  }

  void createCategory() async {
    showLoadingWidget();
    final response = await categoryProvider.createCategory(nameController.text);
    closeLoadingWidget();
    if (response.body != null) {
      categories.add(response.body);
      Get.back();
      showSuccessMessage("Berhasil membuat kategori baru!");
    }
  }

  Future<void> deleteCategory(Category category) async {
    showLoadingWidget();
    final response = await categoryProvider.delete("categories/${category.id}");
    closeLoadingWidget();
    if (response.isOk) {
      showSuccessMessage(response.body["meta"]["message"]);
      categories.remove(category);
    }
  }

  void editCategory(Category category) {
    addOrEditCategory(nameController, this, category: category);
  }

  void updateCategory(Category category) async {
    showLoadingWidget();
    final response = await categoryProvider.put("categories/${category.id}", {
      "name": nameController.text,
    }, headers: {
      "Accept": "application/json"
    });
    closeLoadingWidget();
    if (response.isOk) {
      getCategories();
      Get.back();
      showSuccessMessage(response.body["meta"]["message"]);
    } else {
      print(response.statusCode);
      if(response.statusCode == 400){
        print(response.body);
        showErrorMessage(response.body["data"].toString());
      }
    }
  }

  void selectCategoryPhoto() async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if(pickedFile != null){
      categoryFile = File(pickedFile.path);
      update();
    }
  }
}
