import 'package:borneofood/app/data/models/category_model.dart';
import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/modules/admin/category/controllers/category_controller.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

class CategoryCard extends StatelessWidget {
  final Category category;
  final CategoryController controller;

  const CategoryCard({Key key, this.category, this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            leading: Container(
              height: 40,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                image: DecorationImage(
                  image: NetworkImage(
                    category.imageUrl,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            title: Text(
              category.name,
              style: mainText.copyWith(
                fontSize: 13.0,
              ),
            ),
            subtitle: Text(
              category.name,
              style: mainText.copyWith(
                fontSize: 12.0,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(8.0),
            child: Row(
              children: [
                ActionButton(
                  text: "Edit",
                  bgColor: Colors.amber,
                  onClick: () {
                    controller.editCategory(category);
                  },
                ),
                SizedBox(
                  width: 16,
                ),
                ActionButton(
                  text: "Hapus",
                  bgColor: Colors.red,
                  onClick: () {
                    controller.deleteCategory(category);
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
