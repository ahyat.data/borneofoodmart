import 'package:borneofood/app/data/models/category_model.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/modules/admin/category/controllers/category_controller.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future addOrEditCategory(
  TextEditingController nameController,
  CategoryController controller, {
  Category category,
}) {
  if (category != null) {
    nameController.text = category.name;
  }
  return Get.bottomSheet(
    Container(
      margin: EdgeInsets.all(16.0),
      child: Wrap(
        children: [
          InputForm(
            name: "Nama Kategori",
            hintText: "Nama Kategori",
            controller: nameController,
          ),
          GestureDetector(
            onTap: controller.selectCategoryPhoto,
            child: DottedBorder(
              strokeWidth: 1,
              color: Colors.grey,
              padding: EdgeInsets.all(6),
              dashPattern: [6, 6],
              child: GetBuilder(
                init: controller,
                builder: (value) {
                  return Container(
                    height: 120,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xFFF0F0F0),
                      image: (controller.categoryFile == null)
                          ? null
                          : DecorationImage(
                              image: FileImage(controller.categoryFile),
                              fit: BoxFit.cover),
                    ),
                    child: (controller.categoryFile == null)
                        ? Center(
                            child: Text(
                              "Pilih Foto Ketegori",
                              style: mainText.copyWith(
                                color: Color(0xFF8D92A3),
                                fontSize: 12.0,
                              ),
                            ),
                          )
                        : SizedBox(),
                  );
                },
              ),
            ),
          ),
          ProcessButton(
            title: "Simpan",
            onClick: () async {
              if (category != null) {
                controller.updateCategory(category);
              } else {
                controller.createCategory();
              }
            },
          )
        ],
      ),
    ),
    backgroundColor: Colors.white,
  );
}
