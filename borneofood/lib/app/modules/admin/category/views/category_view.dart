import 'package:borneofood/app/data/models/category_model.dart';
import 'package:borneofood/app/global_widgets/search_field_widget.dart';
import 'package:borneofood/app/global_widgets/shimmer_list_loader.dart';
import 'package:borneofood/app/modules/admin/category/widgets/category_card.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/category_controller.dart';

class CategoryView extends GetWidget<CategoryController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(16.0),
            bottomRight: Radius.circular(16.0),
          ),
        ),
        title: Text(
          "Kategori Manajemen",
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
            child: Container(
              margin: EdgeInsets.all(12.0),
              child: SearchFieldWidget(
                onSearch: (text) {
                  print(text);
                },
                placeHolder: "Cari kategori Yang akan di edit ?",
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: RefreshIndicator(
                onRefresh: () async {
                  controller.getCategories();
                },
                child: Obx(
                  () => (controller.isCategoryLoading.value)
                      ? ListView(children: [ShimmerListLoading()])
                      : ListView.builder(
                          itemCount: controller.categories.length,
                          itemBuilder: (ctx, index) {
                            Category category = controller.categories[index];
                            return CategoryCard(
                              category: category,
                              controller: controller,
                            );
                          },
                        ),
                ),
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          controller.addCategory();
        },
        backgroundColor: mainColor,
      ),
    );
  }
}
