import 'package:borneofood/app/data/providers/food_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AdminController extends GetxController {
  final foodProvider = FoodProvider();

  final pageIndex = 0.obs;
  PageController pageController;
  @override
  void onInit() {
    super.onInit();
    pageController = PageController(initialPage: 0);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  postFood() async {
  }

  void serIndexPage(int index) {
    pageController.animateToPage(index,
        duration: Duration(microseconds: 500), curve: Curves.ease);
    pageIndex(index);
  }
}
