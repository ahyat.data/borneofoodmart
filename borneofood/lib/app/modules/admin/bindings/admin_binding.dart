import 'package:borneofood/app/data/providers/auth_provider.dart';
import 'package:borneofood/app/data/providers/category_provider.dart';
import 'package:borneofood/app/data/providers/food_provider.dart';
import 'package:borneofood/app/modules/admin/category/controllers/category_controller.dart';
import 'package:borneofood/app/modules/admin/food/controllers/food_controller.dart';
import 'package:borneofood/app/modules/admin/setting/controllers/setting_controller.dart';
import 'package:borneofood/app/modules/admin/transaction/controllers/transaction_controller.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/auth_service.dart';
import 'package:get/get.dart';

import '../controllers/admin_controller.dart';

class AdminBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AdminController>(
      () => AdminController(),
    );
    Get.lazyPut<CategoryController>(
          () => CategoryController(),
    );

    Get.lazyPut<FoodController>(() => FoodController());
    Get.lazyPut<FoodProvider>(() => FoodProvider());
    Get.lazyPut<CategoryProvider>(() => CategoryProvider());

    Get.lazyPut<TransactionController>(() => TransactionController());


    Get.lazyPut(() => AuthService());
    Get.put(() => AuthController());
    Get.lazyPut(() => AuthProvider());
    Get.lazyPut<SettingController>(() => SettingController());
  }
}
