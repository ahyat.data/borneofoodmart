import 'package:borneofood/app/data/models/transaction_model.dart';
import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/modules/admin/transaction/controllers/transaction_controller.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

class TransactionDetailView extends GetView<TransactionController> {
  final Transaction transaction;

  TransactionDetailView(this.transaction);

  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "${transaction.invoice}",
      body: Container(
        margin: EdgeInsets.all(8.0),
        child: Column(
          children: [
            Card(
              child: Container(
                margin: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          transaction.createdAt,
                          overflow: TextOverflow.ellipsis,
                          style: mainText.copyWith(),
                        ),
                        buildChip(transaction),
                      ],
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    ...transaction.items.map((e) {
                      return ListTile(
                        contentPadding: EdgeInsets.all(0),
                        dense: true,
                        leading: Image.network(
                          e.photoUrl,
                          width: 60,
                          height: 40,
                          fit: BoxFit.cover,
                        ),
                        title: Text(
                          e.name,
                          style: mainText.copyWith(
                            fontSize: 14.0,
                          ),
                        ),
                        subtitle: Text(
                          "${e.qty.toString()} Buah",
                          style: mainText,
                        ),
                      );
                    }).toList(),
                    Divider(
                      thickness: 1,
                    ),
                    Text(
                      "Alamat Pengiriman",
                      style: mainText,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    Table(
                      children: [
                        TableRow(children: [
                          Text(
                            "Nama : ",
                            style: mainText,
                          ),
                          Text("${transaction.address.receiverName}",
                              style: mainText)
                        ]),
                        TableRow(children: [
                          Text("No Hp :", style: mainText),
                          Text("${transaction.address.phone}", style: mainText)
                        ]),
                        TableRow(children: [
                          Text("Kode Pos : ", style: mainText),
                          Text("${transaction.address.postalCode}",
                              style: mainText)
                        ]),
                        TableRow(children: [
                          Text("Alamat Lengkap :", style: mainText),
                          Text("${transaction.address.address}",
                              style: mainText)
                        ]),
                        TableRow(children: [
                          Text("Kota Asal :", style: mainText),
                          Text("${transaction.address.city}",
                              style: mainText)
                        ]),
                      ],
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    Text(
                      "Metode Pengiriman",
                      style: mainText,
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    Table(
                      children: [
                        TableRow(children: [
                          Text(
                            "Nama Pengiriman : ",
                            style: mainText,
                          ),
                          Text("${transaction.delivery.name}",
                              style: mainText)
                        ]),
                        TableRow(children: [
                          Text("Biaya :", style: mainText),
                          Text("${formatCurrency(transaction.delivery.price)}", style: mainText)
                        ])
                      ],
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Total Belanja",
                                style: mainText.copyWith(
                                  fontSize: 12,
                                ),
                              ),
                              Text(
                                "${formatCurrency(transaction.totalPrice)}",
                                style: mainText.copyWith(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        ActionButton(
                          onClick: () {
                            controller.changeStatus(transaction);
                          },
                          text: "Ubah Status",
                          bgColor: Colors.green,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildChip(Transaction transaction) {
    String text;
    Color bgColor, fgColor;
    switch (transaction.status) {
      case "NEW_ORDER":
        text = "Pesanan Baru";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "PENDING":
        text = "Belum Bayar";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "ON_PROCESS":
        text = "Sedang Diproses";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "CANCELLED":
        text = "Dibatalkan";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "DONE":
        text = "Transaksi Selesai";
        bgColor = Colors.green;
        fgColor = Colors.white;
        break;
      case "SENDING":
        text = "Sedang Dikirim";
        bgColor = Colors.green;
        fgColor = Colors.white;
        break;
    }
    return Chip(
      backgroundColor: bgColor,
      label: Text(
        text,
        style: mainText.copyWith(
          fontSize: 12,
          color: fgColor,
        ),
      ),
      padding: EdgeInsets.all(2.0),
    );
  }
}
