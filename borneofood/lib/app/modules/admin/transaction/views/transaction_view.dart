import 'package:borneofood/app/data/models/transaction_model.dart';
import 'package:borneofood/app/global_widgets/action_button.dart';
import 'package:borneofood/app/global_widgets/search_field_widget.dart';
import 'package:borneofood/app/global_widgets/shimmer_list_loader.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/transaction_controller.dart';

class TransactionView extends GetView<TransactionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(16.0),
            bottomRight: Radius.circular(16.0),
          ),
        ),
        title: Text(
          "Manajemen Transaksi",
          style: mainText.copyWith(
            fontSize: 16.0,
            fontWeight: FontWeight.w100,
            // fontStyle: FontStyle.italic,
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
            child: Container(
              margin: EdgeInsets.all(12.0),
              child: SearchFieldWidget(
                onSearch: (text) {
                  controller.getByInvoice(text);
                },
                placeHolder: "Masukan Invoice yang akan di cari ?",
              ),
            ),
          ),

          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: RefreshIndicator(
                onRefresh: () async {
                  controller.getAll();
                },
                child: Obx(
                  () => (controller.isLoadTransaction.value)
                      ? ListView(
                          children: [ShimmerListLoading()],
                        )
                      :  ListView.builder(
                          itemCount: controller.transactions.length,
                          itemBuilder: (ctx, index) {
                            Transaction transaction =
                                controller.transactions[index];
                            return Card(
                              child: Container(
                                margin: EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          transaction.createdAt,
                                          overflow: TextOverflow.ellipsis,
                                          style: mainText.copyWith(),
                                        ),
                                        buildChip(transaction),
                                      ],
                                    ),
                                    Divider(
                                      thickness: 1,
                                    ),
                                    ...transaction.items.map((e){
                                      return ListTile(
                                        contentPadding: EdgeInsets.all(0),
                                        dense: true,
                                        leading: Image.network(
                                          e.photoUrl,
                                          width: 60,
                                          height: 40,
                                          fit: BoxFit.cover,
                                        ),
                                        title: Text(
                                          e.name,
                                          style: mainText.copyWith(
                                            fontSize: 14.0,
                                          ),
                                        ),
                                        subtitle: Text(
                                          "${e.qty.toString()} Buah",
                                          style: mainText,
                                        ),
                                      );
                                    }).toList(),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Total Belanja",
                                                style: mainText.copyWith(
                                                  fontSize: 12,
                                                ),
                                              ),
                                              Text(
                                                "${formatCurrency(transaction.totalPrice)}",
                                                style: mainText.copyWith(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        ActionButton(
                                          onClick: () {
                                            controller
                                                .changeStatus(transaction);
                                          },
                                          text: "Ubah Status",
                                          bgColor: Colors.green,
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        ActionButton(
                                          onClick: () {
                                            controller
                                                .showDetail(transaction);
                                          },
                                          text: "Detail",
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  buildChip(Transaction transaction) {
    String text;
    Color bgColor, fgColor;
    switch (transaction.status) {
      case "NEW_ORDER":
        text = "Pesanan Baru";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "PENDING":
        text = "Belum Bayar";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "ON_PROCESS":
        text = "Sedang Diproses";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "CANCELLED":
        text = "Dibatalkan";
        bgColor = Colors.yellow;
        fgColor = mainColor;
        break;
      case "DONE":
        text = "Transaksi Selesai";
        bgColor = Colors.green;
        fgColor = Colors.white;
        break;
      case "SENDING":
        text = "Sedang Dikirim";
        bgColor = Colors.green;
        fgColor = Colors.white;
        break;
    }
    return Chip(
      backgroundColor: bgColor,
      label: Text(
        text,
        style: mainText.copyWith(
          fontSize: 12,
          color: fgColor,
        ),
      ),
      padding: EdgeInsets.all(2.0),
    );
  }
}
