import 'package:borneofood/app/data/models/transaction_model.dart';
import 'package:borneofood/app/data/providers/transaction_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/admin/transaction/views/transaction_detail_view.dart';
import 'package:borneofood/app/modules/dashboard/shoping/views/payment_page_view.dart';
import 'package:borneofood/app/theme/color_theme.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TransactionController extends GetxController
    with SingleGetTickerProviderMixin {
  final transactions = <Transaction>[].obs;
  final TransactionProvider transactionProvider =
      Get.put(TransactionProvider());

  TabController tabController;

  final isLoadTransaction = false.obs;

  @override
  void onInit() {
    super.onInit();
    tabController = TabController(length: 5, vsync: this);
  }

  @override
  void onReady() {
    super.onReady();
    setIndex(0);
  }

  @override
  void onClose() {}

  Future<void> setIndex(int index) async {
    switch (index) {
      case 0:
        print("Belum Bayar");
        isLoadTransaction(true);
        final response = await transactionProvider.getAll(status: "NEW_ORDER");
        isLoadTransaction(false);
        if (response.isOk) {
          final data = (response.body["data"] as Iterable)
              .map((e) => Transaction.fromJson(e))
              .toList();
          transactions(data);
        }
        break;
      case 1:
        isLoadTransaction(true);
        final response = await transactionProvider.getAll(status: "ON_PROCESS");
        isLoadTransaction(false);
        if (response.isOk) {
          final data = (response.body["data"] as Iterable)
              .map((e) => Transaction.fromJson(e))
              .toList();
          transactions(data);
        }
        break;

      case 2:
        isLoadTransaction(true);
        final response = await transactionProvider.getAll(status: "SENDING");
        isLoadTransaction(false);
        if (response.isOk) {
          final data = (response.body["data"] as Iterable)
              .map((e) => Transaction.fromJson(e))
              .toList();
          transactions(data);
        }
        break;

      case 3:
        isLoadTransaction(true);
        final response = await transactionProvider.getAll(status: "DONE");
        isLoadTransaction(false);
        if (response.isOk) {
          final data = (response.body["data"] as Iterable)
              .map((e) => Transaction.fromJson(e))
              .toList();
          transactions(data);
        }
        break;
      case 4:
        isLoadTransaction(true);
        final response = await transactionProvider.getAll(status: "CANCELLED");
        isLoadTransaction(false);
        if (response.isOk) {
          final data = (response.body["data"] as Iterable)
              .map((e) => Transaction.fromJson(e))
              .toList();
          transactions(data);
        }
        break;
      // case 5:
      //   isLoadTransaction(true);
      //   final response = await transactionProvider.getAll(status: "CANCELLED");
      //   isLoadTransaction(false);
      //   if (response.isOk) {
      //     final data = (response.body["data"] as Iterable)
      //         .map((e) => Transaction.fromJson(e))
      //         .toList();
      //     transactions(data);
      //   }
      //   break;

      default:
        break;
    }
  }

  void bayarTransaction(Transaction element) async {
    await Get.to(() => PaymentPageView(
          url: element.paymentUrl,
        ));
    setIndex(0);
  }

  Future<void> cancelTransaction(Transaction element) async {
    showLoadingWidget();
    final response = await transactionProvider.cancelTransaction(
      element.id,
    );
    closeLoadingWidget();
    if (response.isOk) {
      showSuccessMessage(response.body["meta"]["message"]);
      setIndex(0);
    }
    print(response.body);
  }

  void setTransactionDone(Transaction element) {
    Get.defaultDialog(
        radius: 0.0,
        title: "Peringatan",
        titleStyle: mainText,
        middleText: "Pastikan Produk sudah diterima",
        middleTextStyle: mainText,
        buttonColor: mainColor,
        cancelTextColor: Colors.red,
        confirmTextColor: Colors.white,
        onConfirm: () async {
          Get.back();
          showLoadingWidget();
          final response = await transactionProvider.doneTransaction(
            element.id,
          );
          closeLoadingWidget();
          if (response.isOk) {
            showSuccessMessage(response.body["meta"]["message"]);
            setIndex(4);
            tabController.animateTo(4);
          }
        },
        onCancel: () {});
  }

  void getByInvoice(String text) {}

  getAll() async {
    isLoadTransaction(true);
    final response = await transactionProvider.getAll();
    isLoadTransaction(false);
    if (response.isOk) {
      final data = (response.body["data"] as Iterable)
          .map((e) => Transaction.fromJson(e))
          .toList();
      transactions(data);
    }
  }

  Future<void> changeStatus(Transaction transaction) async {
    final listStatus = [
      {"name": "Sedang Dikirim", "db": "SENDING"},
      {"name": "Pesanan Ditunda", "db": "PENDING"},
      {"name": "Selesai", "db": "DONE"}
    ];
    final status = await Get.bottomSheet(
      Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Pilih Status",
                  style: mainText,
                ),
                IconButton(
                    splashRadius: 24,
                    icon: Icon(Icons.close),
                    onPressed: () {
                      Get.back();
                    }),
              ],
            ),
          ),
          Divider(
            height: 2,
          ),
          Expanded(
              child: ListView(
            children: listStatus.map((e) {
              return ListTile(
                title: Text(
                  e["name"],
                  style: mainText.copyWith(
                    fontSize: 14,
                  ),
                ),
                onTap: () {
                  Get.back(result: e["db"]);
                },
              );
            }).toList(),
          ))
        ],
      ),
      backgroundColor: Colors.white,
      isDismissible: false,
    );
    if (status != null) {
      showLoadingWidget();
      final response = await transactionProvider.updateStatus(
          status: status, transactionId: transaction.id);
      closeLoadingWidget();
      if (response.isOk) {
        showSuccessMessage("Status Pesanan Berhasil Di Rubah");
        getAll();
      } else {
        showErrorMessage(response.body["meta"]["message"]);
      }
    }
  }

  void showDetail(Transaction transaction) {
    Get.to(() => TransactionDetailView(transaction));
  }
}
