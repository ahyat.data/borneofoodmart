import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:get/get.dart';

class UserSettingController extends GetxController{
  final AuthController authController = Get.find();

  void updateUser() {
    (authController.authState as AuthenticationAuthed).user.name = "Nama Baru";
  }
}