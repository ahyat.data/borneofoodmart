import 'package:borneofood/app/modules/admin/setting/views/user_setting_view.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:get/get.dart';

class SettingController extends GetxController {
  final AuthController authController = Get.find();

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;

  void edit() {
    Get.to(UserSettingView());
  }
}
