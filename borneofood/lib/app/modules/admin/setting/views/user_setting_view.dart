import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/modules/admin/setting/controllers/user_setting_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserSettingView extends StatelessWidget {
  final controller = Get.put(UserSettingController());
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "User Setting View",
      body: Column(
        children: [
          ElevatedButton(
              onPressed: () {
                controller.updateUser();
                Get.back();
              },
              child: Text("Text")),
        ],
      ),
    );
  }
}
