import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/menu_button.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/setting_controller.dart';

class SettingView extends GetView<SettingController> {
  final AuthController authController = Get.find();

  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Pengaturan",
      body: Column(
        children: [
          SizedBox(
            height: 32,
          ),
          Obx(() => buildBodyWidget(authController.authState.value),),
        ],
      ),
    );
  }

  buildBodyWidget(AuthenticationState authState) {
    if (authState is AuthenticationAuthed) {
      var user = authState.user;
      return Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            Container(
              width: 120,
              height: 120,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFF0F0F0),
                  image: DecorationImage(
                    image: NetworkImage(user.profileUrl),
                    fit: BoxFit.cover,
                  )),
            ),
            SizedBox(
              height: 8,
            ),
            Text(user.name),
            SizedBox(
              height: 24,
            ),
            MenuButton(
              text: "Edit Profile",
              icon: Icons.person,
              onPressed: (){
                authController.editProfile();
              },
            ),
            MenuButton(
              text: "Manage Banner",
              icon: Icons.line_style_outlined,
              onPressed: (){
                Get.toNamed(Routes.BANNER);
              },
            ),
            (user.role == "owner") ? MenuButton(
              text: "Manage User",
              icon: Icons.person,
            ): SizedBox(),
            MenuButton(
              text: "Logout",
              icon: Icons.logout,
              onPressed: (){
                authController.logout();
              },
            ),
          ],
        ),
      );
    } else {
      return Column(
        children: [
          Text("Belum Register"),
        ],
      );
    }
  }
}
