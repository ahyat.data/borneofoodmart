import 'package:borneofood/app/modules/admin/setting/controllers/user_setting_controller.dart';
import 'package:get/get.dart';

import '../controllers/setting_controller.dart';

class SettingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SettingController>(
      () => SettingController(),
    );
    Get.lazyPut<UserSettingController>(
          () => UserSettingController(),
    );
  }
}
