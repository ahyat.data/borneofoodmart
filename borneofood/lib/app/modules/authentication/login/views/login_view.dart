import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Login",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(
              'assets/logo.png',
              height: 120,
            ),
            SizedBox(
              height: 16.0,
            ),
            InputForm(
              name: "Alamat Email",
              hintText: "Email yang telah terdaftar",
              type: TextInputType.emailAddress,
              controller: controller.emailController,
            ),
            InputForm(
              name: "Password",
              hintText: "password",
              type: TextInputType.visiblePassword,
              obscureText: true,
              controller: controller.passwordController,
            ),
            ProcessButton(
              title: "Login dan Belanja Sekarang",
              onClick: (){
                controller.login();
              },
            ),
            ProcessButton(
              title: "Daftar Sebagai Member",
              outline: true,
              onClick: () {
                Get.toNamed(Routes.REGISTER);
              },
            ),

          ],
        ),
      ),
    );
  }
}
