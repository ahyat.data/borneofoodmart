import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/data/providers/auth_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:borneofood/app/modules/dashboard/views/first_add_address_view.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  AuthProvider authProvider = Get.find();
  AuthController authController = Get.find();
  final isLoading = false.obs;
  TextEditingController emailController;
  TextEditingController passwordController;

  @override
  void onInit() {
    super.onInit();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
  }

  login() async {
    showLoadingWidget();
    isLoading(true);
    final data = await authProvider.login(
      emailController.text,
      passwordController.text,
    );
    print(data);
    closeLoadingWidget();
    if (data.isOk) {
      User user = User.fromJson(data.body["data"]);
      final box = GetStorage();
      box.write('api_token', data.body["data"]["api_token"]);
      authController.authState.value = AuthenticationAuthed(user: user);
      if (user.role == "user") {
        if (user.hasAddress) {
          Get.offAllNamed(Routes.DASHBOARD);
        } else {
          Get.to(() => FirstAddAddressView());
        }
      } else {
        Get.offAllNamed(Routes.ADMIN);
      }
    } else {
      showErrorMessage(data.body["meta"]["message"]);
    }
  }
}
