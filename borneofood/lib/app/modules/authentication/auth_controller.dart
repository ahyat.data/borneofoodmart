import 'dart:io';
import 'package:borneofood/app/data/models/address_model.dart';
import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/data/providers/auth_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/authentication/auth_service.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/add_address_view.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/add_edit_address_view.dart';
import 'package:borneofood/app/modules/dashboard/profile/views/editprofile_view.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class AuthController extends GetxController {
  File avatarFile;
  final AuthProvider authProvider = Get.find();
  AuthService authService = Get.find();
  RxList<AddressModel> addresses = RxList<AddressModel>();
  final authState = AuthenticationState().obs;

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> signOut() async {
    await authService.logout();
    authState(AuthenticationUnAuthed());
  }

  Future<void> fetchUser() async {
    authState(AuthenticationLoading());
    final user = await authService.fetchUser();
    if (user != null) {
      authState.value = AuthenticationAuthed(user: user);
    } else {
      authState.value = AuthenticationUnAuthed();
    }
  }

  void logout() async {
    showLoadingWidget();
    await authService.logout();
    authState(AuthenticationUnAuthed());
    closeLoadingWidget();
    Get.offAllNamed(Routes.HOME);
  }

  Future<void> selectAvatarFile() async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      avatarFile = File(pickedFile.path);
      update();
    }
  }

  Future<void> editUser(User user, String name, String email) async {
    showLoadingWidget();
    final response =
        await authProvider.editUser(user.id, name, email, file: avatarFile);
    closeLoadingWidget();
    print(response.body);
    if (response.isOk) {
      final user = User.fromJson(response.body["data"]);
      authState(AuthenticationAuthed(user: user));
      Get.back();
    } else {
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  Future<void> updatePassword({
    String current,
    String newPass,
    String repeatPass,
  }) async {
    showLoadingWidget();
    final response = await authProvider.updatePassword(
        current: current, newPass: newPass, repeatPass: repeatPass);
    closeLoadingWidget();
    print(response.body);
    if (response.isOk) {
      final user = User.fromJson(response.body["data"]);
      authState(AuthenticationAuthed(user: user));
      showSuccessMessage(response.body["meta"]["message"]);
    } else {
      showErrorMessage(response.body["meta"]["message"]);
    }
  }

  void editProfile() {
    if (authState.value is AuthenticationAuthed) {
      final user = (authState.value as AuthenticationAuthed).user;
      Get.to(() => EditProfileView(user));
    }
  }

  void editAddress() {
    Get.to(() => AddAddressView());
  }

  void updateAddress({AddressModel address}) {
    Get.to(() => AddEditAddressView(address));
  }
}
