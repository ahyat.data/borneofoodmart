import 'package:borneofood/app/data/models/user_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class AuthenticationState extends Equatable {
  const AuthenticationState();
  @override
  List<Object> get props => [];
}

class AuthenticationLoading extends AuthenticationState {}

class AuthenticationAuthed extends AuthenticationState {
  final User user;
  AuthenticationAuthed({@required this.user});
  @override
  List<Object> get props => [user];
}

class AuthenticationUnAuthed extends AuthenticationState {}
