import 'package:borneofood/app/global_widgets/general_view.dart';
import 'package:borneofood/app/global_widgets/input_form.dart';
import 'package:borneofood/app/global_widgets/process_button.dart';
import 'package:borneofood/app/theme/text_theme.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:get/get.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    return GeneralView(
      title: "Registrasi Member",
      body: Container(
        margin: EdgeInsets.all(16.0),
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                controller.selectAvatarFile();
              },
              child: GetBuilder(
                init: controller,
                builder:(value){
                  return  Container(
                    height: 120,
                    width: 120,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        DottedBorder(
                          child: Container(
                            width: 110,
                            height: 110,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFF0F0F0),
                                image: (controller.avatarFile == null)
                                    ? null
                                    : DecorationImage(
                                    image: FileImage(controller.avatarFile),
                                    fit: BoxFit.cover)),
                            child: (controller.avatarFile == null)
                                ? Center(
                                child: Text(
                                  "Upload\nPhoto",
                                  textAlign: TextAlign.center,
                                  style: mainText.copyWith(
                                    color: Color(0xFF8D92A3),
                                    fontSize: 12.0,
                                  ),
                                ))
                                : SizedBox(),
                          ),
                          padding: EdgeInsets.all(6),
                          borderType: BorderType.RRect,
                          radius: Radius.circular(60),
                          strokeWidth: 1,
                          color: Colors.grey,
                          dashPattern: [6,6],
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            SizedBox(
              height: 16.0,
            ),
            InputForm(
              controller: controller.nameController,
              name: "Nama Lengkap",
              hintText: "Nama Lengkap",
              type: TextInputType.text,
            ),
            InputForm(
              controller: controller.emailController,
              name: "Alamat Email",
              hintText: "Alamat Email Aktif",
              type: TextInputType.emailAddress,
            ),
            InputForm(
              controller: controller.passwordController,
              name: "Password",
              hintText: "password",
              obscureText: true,
              type: TextInputType.text,
            ),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: InputForm(
                controller: controller.passwordConfirmController,
                name: "Password Confirmation",
                hintText: "password confirmation",
                obscureText: true,
              ),
            ),
            ProcessButton(
              title: "Daftar Sebagai Member",
              outline: false,
              onClick: () {
                controller.registerUser();
              },
            ),
          ],
        ),
      ),
    );
  }
}
