import 'dart:io';

import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/data/providers/auth_provider.dart';
import 'package:borneofood/app/global_widgets/show_snackbar.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/dashboard/views/first_add_address_view.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:borneofood/app/utils/global_function.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';

import '../../authentication_state.dart';

class RegisterController extends GetxController {
  File avatarFile;
  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController passwordController;
  TextEditingController passwordConfirmController;
  final AuthProvider authProvider = Get.find();
  AuthController authController = Get.find();

  @override
  void onInit() {
    nameController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    passwordConfirmController = TextEditingController();

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> selectAvatarFile() async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      avatarFile = File(pickedFile.path);
      update();
    }
  }

  void registerUser() async {
    showLoadingWidget();
    final response = await authProvider.register(
      email: emailController.text,
      name: nameController.text,
      password: passwordController.text,
      passwordConfirmation: passwordConfirmController.text,
      file: avatarFile
    );
    closeLoadingWidget();
    if(response.isOk){
      User user = User.fromJson(response.body["data"]);
      print(response.body);
      final box = GetStorage();
      box.write('api_token', response.body["data"]["api_token"]);
      authController.authState.value = AuthenticationAuthed(user: user);
      print(response.body);
      if (user.role == "user") {
        if(user.hasAddress){
          Get.offAllNamed(Routes.DASHBOARD);
        }else{
          Get.to(() => FirstAddAddressView());
        }
      } else {
        Get.offAllNamed(Routes.ADMIN);
      }
      showSuccessMessage(response.body["meta"]["message"]);
    }else{
      showErrorMessage(response.body["meta"]["message"]);
    }
  }
}
