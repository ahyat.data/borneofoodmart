import 'package:borneofood/app/data/models/user_model.dart';
import 'package:borneofood/app/data/providers/auth_provider.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AuthService extends GetxService {
  final box = GetStorage();
  final AuthProvider authProvider = Get.find();


  Future<User> fetchUser() async {
    final token = box.read('api_token');
    if (token == null) {
      await Future.delayed(Duration(seconds: 5));
      print("Setelah Secon");
      return null;
    }
    final response =  await authProvider.fetch();
    if(response.isOk){
      return User.fromJson(response.body["data"]);
    }else{
      return null;
    }
  }

  Future<void> logout() async {
    box.remove('api_token');
    return null;
  }
}
