import 'package:borneofood/app/data/providers/auth_provider.dart';
import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/auth_service.dart';
import 'package:get/get.dart';

import '../controllers/splash_controller.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SplashController>(
      () => SplashController(),
    );
  }
}
