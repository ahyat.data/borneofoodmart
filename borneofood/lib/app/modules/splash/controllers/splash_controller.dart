import 'package:borneofood/app/modules/authentication/auth_controller.dart';
import 'package:borneofood/app/modules/authentication/authentication_state.dart';
import 'package:borneofood/app/modules/dashboard/views/first_add_address_view.dart';
import 'package:borneofood/app/routes/app_pages.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  final AuthController authController = Get.find();
  @override
  void onInit() {
    super.onInit();
    checkAuth();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void checkAuth() async {
    await authController.fetchUser();
    if(authController.authState.value is AuthenticationUnAuthed){
      Get.offAllNamed(Routes.HOME);
    }else{
      final user = (authController.authState.value as AuthenticationAuthed).user;
      final role = user.role;
      print(role);
      if(role == 'user'){
        if(user.hasAddress){
          Get.offAllNamed(Routes.DASHBOARD);
        }else{
          Get.offAll(() => FirstAddAddressView());
        }

      }else{
        Get.offAllNamed(Routes.ADMIN);
      }
    }
  }
}
