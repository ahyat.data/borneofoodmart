import 'package:get/get.dart';

import 'package:borneofood/app/modules/admin/banner/bindings/banner_binding.dart';
import 'package:borneofood/app/modules/admin/banner/views/banner_view.dart';
import 'package:borneofood/app/modules/admin/bindings/admin_binding.dart';
import 'package:borneofood/app/modules/admin/category/bindings/category_binding.dart';
import 'package:borneofood/app/modules/admin/category/views/category_view.dart';
import 'package:borneofood/app/modules/admin/food/bindings/food_binding.dart';
import 'package:borneofood/app/modules/admin/food/views/add_food_view.dart';
import 'package:borneofood/app/modules/admin/food/views/food_view.dart';
import 'package:borneofood/app/modules/admin/setting/bindings/setting_binding.dart';
import 'package:borneofood/app/modules/admin/setting/views/setting_view.dart';
import 'package:borneofood/app/modules/admin/transaction/bindings/transaction_binding.dart';
import 'package:borneofood/app/modules/admin/transaction/views/transaction_view.dart';
import 'package:borneofood/app/modules/admin/views/admin_view.dart';
import 'package:borneofood/app/modules/authentication/login/bindings/login_binding.dart';
import 'package:borneofood/app/modules/authentication/login/views/login_view.dart';
import 'package:borneofood/app/modules/authentication/register/bindings/register_binding.dart';
import 'package:borneofood/app/modules/authentication/register/views/register_view.dart';
import 'package:borneofood/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:borneofood/app/modules/dashboard/faq/bindings/faq_binding.dart';
import 'package:borneofood/app/modules/dashboard/faq/views/faq_view.dart';
import 'package:borneofood/app/modules/dashboard/views/dashboard_view.dart';
import 'package:borneofood/app/modules/home/bindings/home_binding.dart';
import 'package:borneofood/app/modules/home/views/home_view.dart';
import 'package:borneofood/app/modules/splash/bindings/splash_binding.dart';
import 'package:borneofood/app/modules/splash/views/splash_view.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
      children: [
        GetPage(
          name: _Paths.FAQ,
          page: () => FaqView(),
          binding: FaqBinding(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.ADMIN,
      page: () => AdminView(),
      binding: AdminBinding(),
      children: [
        GetPage(
            name: _Paths.FOOD,
            page: () => FoodView(),
            binding: FoodBinding(),
            children: [
              GetPage(
                name: _Paths.ADD,
                page: () => AddFoodView(),
              )
            ]),
        GetPage(
          name: _Paths.CATEGORY,
          page: () => CategoryView(),
          binding: CategoryBinding(),
        ),
        GetPage(
          name: _Paths.TRANSACTION,
          page: () => TransactionView(),
          binding: TransactionBinding(),
        ),
        GetPage(
          name: _Paths.SETTING,
          page: () => SettingView(),
          binding: SettingBinding(),
        ),
        GetPage(
          name: _Paths.BANNER,
          page: () => BannerView(),
          binding: BannerBinding(),
        ),
      ],
    ),
  ];
}
