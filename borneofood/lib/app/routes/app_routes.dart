part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const SPLASH = _Paths.SPLASH;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const SHOPING = _Paths.DASHBOARD + _Paths.SHOPING;
  static const INFO = _Paths.DASHBOARD + _Paths.INFO;
  static const CART = _Paths.DASHBOARD + _Paths.CART;
  static const PROFILE = _Paths.DASHBOARD + _Paths.PROFILE;
  static const ADMIN = _Paths.ADMIN;
  static const FOOD = _Paths.ADMIN + _Paths.FOOD;
  static const CATEGORY = _Paths.ADMIN + _Paths.CATEGORY;
  static const TRANSACTION = _Paths.ADMIN + _Paths.TRANSACTION;
  static const FOOD_ADD = _Paths.ADMIN + _Paths.FOOD + _Paths.ADD;
  static const SETTING = _Paths.ADMIN + _Paths.SETTING;
  static const BANNER = _Paths.ADMIN + _Paths.BANNER;
  static const FAQ = _Paths.DASHBOARD + _Paths.FAQ;
}

abstract class _Paths {
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const SPLASH = '/splash';
  static const DASHBOARD = '/dashboard';
  static const SHOPING = '/shoping';
  static const INFO = '/info';
  static const CART = '/cart';
  static const PROFILE = '/profile';
  static const ADMIN = '/admin';
  static const FOOD = '/food';
  static const CATEGORY = '/category';
  static const TRANSACTION = '/transaction';
  static const ADD = '/add';
  static const SETTING = '/setting';
  static const BANNER = '/banner';
  static const FAQ = '/faq';
}
